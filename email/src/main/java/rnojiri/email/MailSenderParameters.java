package rnojiri.email;

import java.util.Properties;

/**
 * Defines all email sender configuration parameters.
 * 
 * @author rnojiri
 */
public class MailSenderParameters
{
	private Properties connectionProperties;
	
	private String hostName;
	
	private String userName;
	
	private String password;
	
	public MailSenderParameters()
	{
		this.connectionProperties = new Properties();
		this.connectionProperties.put("mail.transport.protocol", "smtp");
		this.connectionProperties.put("mail.smtp.port", 587);
		this.connectionProperties.put("mail.smtp.auth", true);
		this.connectionProperties.put("mail.smtp.starttls.enable", true);
		this.connectionProperties.put("mail.smtp.starttls.required", true);
		this.connectionProperties.put("mail.mime.charset", "utf-8");
	}

    /**
	 * @return the connectionProperties
	 */
	public Properties getConnectionProperties()
	{
		return connectionProperties;
	}

	/**
	 * @return the hostName
	 */
	public String getHostName()
	{
		return hostName;
	}

	/**
	 * @param hostName the hostName to set
	 */
	public void setHostName(String hostName)
	{
		this.hostName = hostName;
	}

	/**
	 * @return the userName
	 */
	public String getUserName()
	{
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * Sets the SMTP port (default: 587).
	 * 
	 * @param smtpPort the smtpPort to set
	 */
	public void setSmtpPort(int smtpPort)
	{
		this.connectionProperties.put("mail.smtp.port", smtpPort);
	}
	
	/**
	 * Enable/Disable the SMTP authentication (default: true)
	 * 
	 * @param smtpAuth the smtpAuth to set
	 */
	public void setSmtpAuth(boolean smtpAuth)
	{
		this.connectionProperties.put("mail.smtp.auth", smtpAuth);
	}
	
	/**
	 * Enable/Disable the SMTP start TLS. (default: true).
	 *  
	 * @param smtpStartTlsEnable the smtpStartTlsEnable to set
	 */
	public void setSmtpStartTlsEnable(boolean smtpStartTlsEnable)
	{
	    this.connectionProperties.put("mail.smtp.starttls.enable", true);
	}
	
	/**
	 * Enable/Disable the SMTP start TLS required. (default: true).
	 * 
	 * @param smtpStartTlsRequired the smtpStartTlsRequired to set
	 */
	public void setSmtpStartTlsRequired(boolean smtpStartTlsRequired)
	{
		this.connectionProperties.put("mail.smtp.starttls.required", smtpStartTlsRequired);
	}
	
	/**
	 * Sets the MIME charset (default: utf-8).
	 * 
	 * @param smtpMimeCharset the smtpMimeCharset to set
	 */
	public void setSmtpMimeCharset(String smtpMimeCharset)
	{
	    this.connectionProperties.put("mail.mime.charset", smtpMimeCharset);
	}
}
