package rnojiri.email;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;

import rnojiri.lib.interfaces.AsyncCallbackInterface;
import rnojiri.lib.interfaces.Loggable;

/**
 * The mail sender interface for all e-mails services available.
 * 
 * @author rnojiri
 */
public abstract class AbstractMailSender implements Loggable
{
	protected ExecutorService executorService;
	
	private static final long RETRY_BASE_TIME = 60000;
	
	private static final long MAX_RETRIES = 5; 
	
	private static final AsyncCallbackInterface DEFAULT_ASYNC_CALLBACK_INTERFACE = new DefaultMailSenderAsyncCallback();
	
	/**
	 * @param maxNumThreads
	 */
	public AbstractMailSender(int maxNumThreads)
	{
		executorService = Executors.newFixedThreadPool(maxNumThreads);
	}
	
	/**
	 * Sends a new e-mail.
	 * 
	 * @param email
	 * @param asynchronously
	 * @return boolean
	 */
	public boolean sendMail(Email email, boolean asynchronously)
	{
		return this.sendMail(email, asynchronously, DEFAULT_ASYNC_CALLBACK_INTERFACE);
	}
	
	/**
	 * Sends a new e-mail.
	 * 
	 * @param email
	 * @param asynchronously
	 * @param errorCallbackInterface
	 * @return boolean
	 */
	public boolean sendMail(Email email, boolean asynchronously, AsyncCallbackInterface errorCallbackInterface)
	{
		Logger logger = getLogger();
		
		if(logger.isDebugEnabled())
		{
			logger.debug("Sending e-mail from \"" + email.getFrom() + "\" to \"" + email.getTo().toString() + "\" " + (asynchronously ? "asynchronously" : "synchronously"));
		}
		
		if(errorCallbackInterface == null)
		{
			errorCallbackInterface = DEFAULT_ASYNC_CALLBACK_INTERFACE;
		}
		
		if(asynchronously)
		{
			executorService.execute(new MailSenderThread(email, errorCallbackInterface));
			
			return true;
		}
		else
		{
			return sendMail(email, errorCallbackInterface);
		}
	}
	
	/**
	 * Sends the e-mail asynchronously.
	 * 
	 * @author rnojiri
	 */
	private class MailSenderThread implements Runnable
	{
		private final Email email;
		
		private final AsyncCallbackInterface asyncCallbackInterface;
		
		public MailSenderThread(final Email email, final AsyncCallbackInterface asyncCallbackInterface)
		{
			this.email = email;
			this.asyncCallbackInterface = asyncCallbackInterface;
		}
		
		@Override
		public void run()
		{
			int tentatives = 0;
			boolean wasSent = false;
			
			do
			{
				wasSent = sendMail(email, asyncCallbackInterface);
				
				if(tentatives > 0)
				{
					getLogger().warn("E-mail send tentative number " + tentatives);
				}
				
				tentatives++;
				
				if(!wasSent)
				{
					if(tentatives > MAX_RETRIES)
					{
						getLogger().warn("E-mail ignored in tentative number " + tentatives);
						
						break;
					}
					
					try
					{
						Thread.sleep(RETRY_BASE_TIME * tentatives);
					}
					catch(InterruptedException e)
					{
						;
					}
				}
			}
			while(!wasSent);
		}
	}
	
	/**
	 * Shutdown the thread executor.
	 */
	public void shutdown()
	{
		executorService.shutdown();
		
		getLogger().info("Shutting down mail sender threads.");
	}
	
	/**
	 * Sends the e-mail implementation.
	 * 
	 * @param email
	 * @param errorCallbackInterface
	 * @return boolean
	 */
	protected abstract boolean sendMail(final Email email, final AsyncCallbackInterface errorCallbackInterface);
}
