package rnojiri.email;

import rnojiri.lib.interfaces.AsyncCallbackInterface;

/**
 * The default error callback interface
 * 
 * @author rnojiri
 */
public class DefaultMailSenderAsyncCallback implements AsyncCallbackInterface
{
	@Override
	public void handleSuccess()
	{
		; //does nothing
	}
	
	@Override
	public void handleException(Exception e)
	{
		; //does nothing
	}
}
