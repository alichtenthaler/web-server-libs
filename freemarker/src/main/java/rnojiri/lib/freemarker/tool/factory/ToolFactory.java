package rnojiri.lib.freemarker.tool.factory;

import javax.servlet.http.HttpServletRequest;

import rnojiri.lib.freemarker.tool.FreemarkerTool;

/**
 * A Freemarker tool factory.
 * 
 * @author rnojiri
 *
 * @param <T>
 */
public interface ToolFactory<T extends FreemarkerTool>
{
	/**
	 * Returns a new or a singleton instance of the tool.
	 * 
	 * @param httpServletRequest
	 * @return T
	 */
	public T getInstance(HttpServletRequest httpServletRequest);
}
