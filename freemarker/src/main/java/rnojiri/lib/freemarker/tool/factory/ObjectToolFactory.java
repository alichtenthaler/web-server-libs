package rnojiri.lib.freemarker.tool.factory;

import javax.servlet.http.HttpServletRequest;

import rnojiri.lib.freemarker.tool.ObjectTool;

/**
 * Object tool factory.
 * 
 * @author rnojiri
 */
public class ObjectToolFactory implements ToolFactory<ObjectTool>
{
	private ObjectTool objectTool;
	
	public ObjectToolFactory()
	{
		objectTool = new ObjectTool();
	}
	
	@Override
	public ObjectTool getInstance(HttpServletRequest httpServletRequest)
	{
		return objectTool;
	}
}
