package rnojiri.lib.freemarker.tool.factory;

import javax.servlet.http.HttpServletRequest;

import rnojiri.lib.freemarker.tool.RequestTool;

/**
 * The RequestVariablesTool factory.
 * 
 * @author rnojiri
 */
public class RequestVariablesToolFactory implements ToolFactory<RequestTool>
{
	@Override
	public RequestTool getInstance(HttpServletRequest httpServletRequest)
	{
		if(httpServletRequest == null) return null;
		
		return new RequestTool(httpServletRequest);
	}
}
