package rnojiri.lib.freemarker.tool;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * An multiple object verifier tool.
 * 
 * @author rnojiri
 */
public class ObjectTool implements FreemarkerTool
{
	private static final String NAME = "objectTool"; 
	
	/**
	 * Checks if an object is null.
	 * 
	 * @param obj
	 * @return boolean
	 */
	public boolean isNull(Object obj)
	{
		return obj == null;
	}
	
	/**
	 * Checks if an object is not null.
	 * 
	 * @param obj
	 * @return boolean
	 */
	public boolean isNotNull(Object obj)
	{
		return obj != null;
	}
	
	/**
	 * Checks if an object is a String.
	 * 
	 * @param obj
	 * @return boolean
	 */
	public boolean isString(Object obj)
	{
		return obj instanceof String;
	}
	
	/**
	 * Checks if an object is a Number.
	 * 
	 * @param obj
	 * @return boolean
	 */
	public boolean isNumber(Object obj)
	{
		return obj instanceof Number;
	}
	
	/**
	 * Checks if an object is a Float.
	 * 
	 * @param obj
	 * @return boolean
	 */
	public boolean isFloat(Object obj)
	{
		return obj instanceof Float;
	}
	
	/**
	 * Checks if an object is a Integer.
	 * 
	 * @param obj
	 * @return boolean
	 */
	public boolean isInteger(Object obj)
	{
		return obj instanceof Integer;
	}
	
	/**
	 * Checks if an object is a Double.
	 * 
	 * @param obj
	 * @return boolean
	 */
	public boolean isDouble(Object obj)
	{
		return obj instanceof Double;
	}
	
	/**
	 * Checks if an object is a Double.
	 * 
	 * @param obj
	 * @return boolean
	 */
	public boolean isLong(Object obj)
	{
		return obj instanceof Long;
	}
	
	/**
	 * Checks if an object is a Date.
	 * 
	 * @param obj
	 * @return boolean
	 */
	public boolean isDate(Object obj)
	{
		return obj instanceof Date;
	}
	
	/**
	 * Checks if an object is a Boolean.
	 * 
	 * @param obj
	 * @return boolean
	 */
	public boolean isBoolean(Object obj)
	{
		return obj instanceof Boolean;
	}
	
	/**
	 * Checks if an object is an array.
	 * 
	 * @param obj
	 * @return boolean
	 */
	public boolean isArray(Object obj)
	{
		return obj.getClass().isArray();
	}
	
	/**
	 * Checks if an object is a collection.
	 * 
	 * @param obj
	 * @return boolean
	 */
	public boolean isCollection(Object obj)
	{
		return obj instanceof Collection<?>;
	}
	
	/**
	 * Checks if an object is a map.
	 * 
	 * @param obj
	 * @return boolean
	 */
	public boolean isMap(Object obj)
	{
		return obj instanceof Map<?, ?>;
	}
	
	@Override
	public String getName()
	{
		return NAME;
	}
}
