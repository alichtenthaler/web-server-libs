package rnojiri.lib.validation.form;

import rnojiri.lib.validation.PostValidation;
import rnojiri.lib.validation.Validable;
import rnojiri.lib.validation.annotation.OptionalParam;

/**
 * A type test form.
 * 
 * @author rnojiri
 */
public class TypeTestForm implements Validable
{

	private int intVar;

	private Integer intCVar;

	private float floatVar;

	private Float floatCVar;

	private long longVar;

	private Long longCVar;

	private boolean booleanVar;

	private Boolean booleanCVar;

	private String stringVar;

	public TypeTestForm()
	{
		;
	}

	/**
	 * @return the intVar
	 */
	@OptionalParam(defaultValue = "1")
	public int getIntVar()
	{
		return intVar;
	}

	/**
	 * @param intVar
	 *            the intVar to set
	 */
	public void setIntVar(int intVar)
	{
		this.intVar = intVar;
	}

	/**
	 * @return the intCVar
	 */
	@OptionalParam(defaultValue = "2")
	public Integer getIntCVar()
	{
		return intCVar;
	}

	/**
	 * @param intCVar
	 *            the intCVar to set
	 */
	public void setIntCVar(Integer intCVar)
	{
		this.intCVar = intCVar;
	}

	/**
	 * @return the floatVar
	 */
	@OptionalParam(defaultValue = "3")
	public float getFloatVar()
	{
		return floatVar;
	}

	/**
	 * @param floatVar
	 *            the floatVar to set
	 */
	public void setFloatVar(float floatVar)
	{
		this.floatVar = floatVar;
	}

	/**
	 * @return the floatCVar
	 */
	@OptionalParam(defaultValue = "4")
	public Float getFloatCVar()
	{
		return floatCVar;
	}

	/**
	 * @param floatCVar
	 *            the floatCVar to set
	 */
	public void setFloatCVar(Float floatCVar)
	{
		this.floatCVar = floatCVar;
	}

	/**
	 * @return the longVar
	 */
	@OptionalParam(defaultValue = "5")
	public long getLongVar()
	{
		return longVar;
	}

	/**
	 * @param longVar
	 *            the longVar to set
	 */
	public void setLongVar(long longVar)
	{
		this.longVar = longVar;
	}

	/**
	 * @return the longCVar
	 */
	@OptionalParam(defaultValue = "6")
	public Long getLongCVar()
	{
		return longCVar;
	}

	/**
	 * @param longCVar
	 *            the longCVar to set
	 */
	public void setLongCVar(Long longCVar)
	{
		this.longCVar = longCVar;
	}

	/**
	 * @return the booleanVar
	 */
	@OptionalParam(defaultValue = "true")
	public boolean isBooleanVar()
	{
		return booleanVar;
	}

	/**
	 * @param booleanVar
	 *            the booleanVar to set
	 */
	public void setBooleanVar(boolean booleanVar)
	{
		this.booleanVar = booleanVar;
	}

	/**
	 * @return the booleanCVar
	 */
	@OptionalParam(defaultValue = "true")
	public Boolean getBooleanCVar()
	{
		return booleanCVar;
	}

	/**
	 * @param booleanCVar
	 *            the booleanCVar to set
	 */
	public void setBooleanCVar(Boolean booleanCVar)
	{
		this.booleanCVar = booleanCVar;
	}

	/**
	 * @return the stringVar
	 */
	@OptionalParam(defaultValue = "test")
	public String getStringVar()
	{
		return stringVar;
	}

	/**
	 * @param stringVar
	 *            the stringVar to set
	 */
	public void setStringVar(String stringVar)
	{
		this.stringVar = stringVar;
	}

	@Override
	public PostValidation<? extends Validable>[] getPostValidations()
	{
		return null;
	}
}
