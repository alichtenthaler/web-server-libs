package rnojiri.lib.validation.form;

import java.util.List;

import rnojiri.lib.validation.PostValidation;
import rnojiri.lib.validation.Validable;
import rnojiri.lib.validation.annotation.RequiredParam;

/**
 * Form list form.
 * 
 * @author rnojiri
 */
public class ListParamForm implements Validable
{
	
	private String parent;
	
	private List<ListItemForm> items;
	
	public ListParamForm()
	{
		;
	}

	/**
	 * @return the parent
	 */
	@RequiredParam
	public String getParent()
	{
		return parent;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(String parent)
	{
		this.parent = parent;
	}

	/**
	 * @return the items
	 */
	@RequiredParam
	public List<ListItemForm> getItems()
	{
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(List<ListItemForm> items)
	{
		this.items = items;
	}

	@Override
	public PostValidation<? extends Validable>[] getPostValidations()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
