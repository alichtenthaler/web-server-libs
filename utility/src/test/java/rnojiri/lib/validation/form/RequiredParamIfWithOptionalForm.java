package rnojiri.lib.validation.form;

import rnojiri.lib.validation.PostValidation;
import rnojiri.lib.validation.Validable;
import rnojiri.lib.validation.annotation.OptionalParam;
import rnojiri.lib.validation.annotation.RequiredParamIf;

/**
 * A form to test RequiredParamIf.
 * 
 * @author rnojiri
 */
public class RequiredParamIfWithOptionalForm implements Validable
{
	
	private String requiredValue;
	
	private String type;
	
	public RequiredParamIfWithOptionalForm()
	{
		;
	}

	/**
	 * @return the requiredValue
	 */
	@RequiredParamIf(parameter = "type", value = "required")
	public String getRequiredValue()
	{
		return requiredValue;
	}

	/**
	 * @param requiredValue the requiredValue to set
	 */
	public void setRequiredValue(String requiredValue)
	{
		this.requiredValue = requiredValue;
	}

	/**
	 * @return the type
	 */
	@OptionalParam(defaultValue = "required")
	public String getType()
	{
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type)
	{
		this.type = type;
	}

	@Override
	public PostValidation<? extends Validable>[] getPostValidations()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
