package rnojiri.lib.validation.form;

import rnojiri.lib.validation.PostValidation;
import rnojiri.lib.validation.Validable;
import rnojiri.lib.validation.annotation.RegexpValidation;
import rnojiri.lib.validation.annotation.RequiredParam;

/**
 * Testes the regular expression form.
 * 
 * @author rnojiri
 */
public class RegexpForm implements Validable
{
	private String regexpVar;
		
	private String notRequiredRegexpVar;
	
	public RegexpForm()
	{
		;
	}

	/**
	 * @return the regexpVar
	 */
	@RequiredParam
	@RegexpValidation("[a-z]+@[a-z]+.com")
	public String getRegexpVar()
	{
		return regexpVar;
	}

	/**
	 * @param regexpVar the regexpVar to set
	 */
	public void setRegexpVar(String regexpVar)
	{
		this.regexpVar = regexpVar;
	}

	/**
	 * @return the notRequiredRegexpVar
	 */
	@RegexpValidation("[a-z]+@[a-z]+.com")
	public String getNotRequiredRegexpVar()
	{
		return notRequiredRegexpVar;
	}

	/**
	 * @param notRequiredRegexpVar the notRequiredRegexpVar to set
	 */
	public void setNotRequiredRegexpVar(String notRequiredRegexpVar)
	{
		this.notRequiredRegexpVar = notRequiredRegexpVar;
	}

	@Override
	public PostValidation<? extends Validable>[] getPostValidations()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
