package rnojiri.lib.support;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A generic configuration reloader.
 * 
 * @author rnojiri
 */
public abstract class ConfigurationReloader
{
	private Logger logger;
	
	private ScheduledExecutorService scheduledExecutorService;
	
	public ConfigurationReloader(long time)
	{
		scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
		
		scheduledExecutorService.scheduleAtFixedRate(new ConfigurationReloaderThread(), time, time, TimeUnit.MILLISECONDS);
		
		logger = LoggerFactory.getLogger(getClass());
		
		logger.info("Starting configuration reloader executor with " + time + " miliseconds.");
	}
	
	/**
	 * Shuts down the executor.
	 */
	public void shutdown()
	{
		scheduledExecutorService.shutdown();
	}
	
	/**
	 * Reloads the configuration.
	 */
	protected abstract void reloadConfiguration();
	
	/**
	 * Configuration reloader thread.
	 * 
	 * @author rnojiri
	 */
	private class ConfigurationReloaderThread implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				if(logger.isDebugEnabled())
				{
					logger.debug("Executing configuration reloader thread.");
				}
				
				reloadConfiguration();
			}
			catch(Exception e)
			{
				logger.error("Error reloading configuration.", e);
			}
		}
	}
}