package rnojiri.lib.support;

/**
 * A ranged value.
 * 
 * @author rnojiri
 */
public class RangedValue<T extends Number>
{
	private final T start;
	
	private final T end;

	public RangedValue(T start, T end)
	{
		this.start = start;
		this.end = end;
	}

	/**
	 * @return the start
	 */
	public T getStart()
	{
		return start;
	}

	/**
	 * @return the end
	 */
	public T getEnd()
	{
		return end;
	}
	
	@Override
	public String toString()
	{
		return "[" + start + ", " + end + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((end == null) ? 0 : end.hashCode());
		result = prime * result + ((start == null) ? 0 : start.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		@SuppressWarnings("unchecked")
		RangedValue<T> other = (RangedValue<T>) obj;
		if (end == null)
		{
			if (other.end != null) return false;
		}
		else if (!end.equals(other.end)) return false;
		if (start == null)
		{
			if (other.start != null) return false;
		}
		else if (!start.equals(other.start)) return false;
		return true;
	}
}
