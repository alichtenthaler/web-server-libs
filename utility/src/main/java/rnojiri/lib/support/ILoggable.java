package rnojiri.lib.support;

import org.slf4j.Logger;

/**
 * Defines a loggable class. 
 * 
 * @author rnojiri
 */
public interface ILoggable
{
	/**
	 * Returns the class logger.
	 * 
	 * @return Logger
	 */
	Logger getLogger();
}
