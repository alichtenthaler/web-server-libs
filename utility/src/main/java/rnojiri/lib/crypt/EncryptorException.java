package rnojiri.lib.crypt;

/**
 * A generic encryptor exception.
 * 
 * @author rnojiri
 */
public class EncryptorException extends RuntimeException
{
	private static final long serialVersionUID = 7527633869894782005L;

	public EncryptorException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public EncryptorException(Throwable cause)
	{
		super(cause);
	}
}
