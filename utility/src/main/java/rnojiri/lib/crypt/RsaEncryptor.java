package rnojiri.lib.crypt;

import java.io.FileReader;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.Cipher;

import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x509.RSAPublicKeyStructure;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.util.io.pem.PemObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Builds encrypted array of bytes using RSA algorithm.
 * 
 * @author rnojiri
 */
@SuppressWarnings("deprecation")
public class RsaEncryptor
{
	private static Logger log = LoggerFactory.getLogger(RsaEncryptor.class);
	
	private Cipher cipher;
	
	/**
	 * Uses the pem file to create the cipher.
	 * 
	 * @param pemFilePath
	 */
	public RsaEncryptor(String pemFilePath)
	{
		PEMParser pemParser = null;

		try
		{
			log.info("Trying to load pem file: \"" + pemFilePath + "\".");
			
			pemParser = new PEMParser(new FileReader(pemFilePath));
			
			log.info("Pem file: \"" + pemFilePath + "\" found.");

			PemObject pemObject = pemParser.readPemObject();
			pemParser.close();

			ASN1Sequence asn1seq = (ASN1Sequence) ASN1Sequence.fromByteArray(pemObject.getContent());

			SubjectPublicKeyInfo pkInfo = SubjectPublicKeyInfo.getInstance(asn1seq);
			RSAPublicKeyStructure pubk = RSAPublicKeyStructure.getInstance(pkInfo.getPublicKey());
			BigInteger exponent = pubk.getPublicExponent();
			BigInteger modulus = pubk.getModulus();
			
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			RSAPublicKeySpec pubKeySpec = new RSAPublicKeySpec(modulus, exponent);
			PublicKey key = (PublicKey) keyFactory.generatePublic(pubKeySpec);

			cipher = Cipher.getInstance("RSA/ECB/NoPadding");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			
			log.info("RSA cipher was created with success!");
		}
		catch (Exception e)
		{
			log.error("Error creating RSA cipher.", e);
			
			throw new EncryptorException(e);
		}
	}
	
	/**
	 * Builds a new encryption.
	 * 
	 * @param phrase
	 * @return byte[]
	 */
	public byte[] encrypt(String phrase)
	{
		if(phrase == null)
		{
			return null;
		}
		
		try
		{
			return cipher.doFinal(phrase.getBytes());
		}
		catch(Exception e)
		{
			log.error("Error encrypting phrase \"" + phrase + "\".", e);
			
			throw new EncryptorException(e);
		}
	}
}
