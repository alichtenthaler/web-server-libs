package rnojiri.lib.collection;

import java.util.ArrayList;

/**
 * A pre-built array list.
 * 
 * @author rnojiri
 *
 * @param <E>
 */
public class PreBuiltArrayList<E> extends ArrayList<E>
{
	private static final long serialVersionUID = 8919422706330916383L;
	
	/**
	 * Inserts all items using the constructor.
	 * 
	 * @param items
	 */
	@SafeVarargs
	public PreBuiltArrayList(E ... items)
	{
		if(items == null)
		{
			throw new NullPointerException();
		}
		
		for(E item : items)
		{
			add(item);
		}
	}
}
