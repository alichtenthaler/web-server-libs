package rnojiri.lib.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import rnojiri.lib.support.Constants;

/**
 * Reflection utilities.
 * 
 * @author rnojiri
 */
public class ReflectionUtil
{
	public static final String GET = "get";
	public static final String SET = "set";
	public static final String IS = "is";
	
	/**
	 * Returns the parsed string value.
	 * 
	 * @param classType
	 * @param strValue
	 * @return Object
	 * @throws UnsupportedTypeException 
	 */
	public static Object parseValue(Class<?> classType, String strValue) throws UnsupportedTypeException
	{
		if(strValue == null)
		{
			return null;
		}
		
		Object value = null;
		
		if(classType.equals(Integer.class) || classType == int.class)
		{
			value = Integer.valueOf(strValue);
		}
		else if(classType.equals(Float.class) || classType == float.class)
		{
			value = Float.valueOf(strValue);
		}
		else if(classType.equals(Double.class) || classType == double.class)
		{
			value = Double.valueOf(strValue);
		}
		else if(classType.equals(Long.class) || classType == long.class)
		{
			value = Long.valueOf(strValue);
		}
		else if(classType.equals(Boolean.class) || classType == boolean.class)
		{
			value = Boolean.valueOf(strValue);
		}
		else if(classType.equals(String.class))
		{
			value = strValue;
		}
		else
		{
			throw new UnsupportedTypeException(classType);
		}
		
		return value;
	}
	
	/**
	 * Returns the method name by field.
	 * 
	 * @param fieldName
	 * @param methodPrefix
	 * @return String
	 */
	public static String getMethodName(String fieldName, String methodPrefix)
	{
		char name[] = fieldName.toCharArray();
		
		StringBuilder stringBuilder = new StringBuilder(Constants.BUFFER_SIZE_32).append(methodPrefix)
														 						 .append(Character.toUpperCase(name[0]));
		
		if(name.length > 1)
		{
			stringBuilder.append(String.copyValueOf(name, 1, name.length - 1));
		}
		
		return stringBuilder.toString();
	}
	
	/**
	 * Returns the field name by its method name.
	 * 
	 * @param methodName
	 * @return String
	 */
	public static String getFieldNameByMethod(String methodName)
	{
		char name[] = methodName.toCharArray();
		
		name[3] = Character.toLowerCase(name[3]);
		
		StringBuilder stringBuilder = new StringBuilder(Constants.BUFFER_SIZE_32).append(String.copyValueOf(name, 3, name.length - 3));
		
		return stringBuilder.toString();
	}
	
	/**
	 * Returns the field value.
	 * 
	 * @param object
	 * @param fieldName
	 * @return Object
	 * @throws FieldIsNotAccessibleException 
	 */
	public static Object getFieldValue(Object object, String fieldName) throws FieldIsNotAccessibleException
	{
		Object value = null;
		
		try
		{
			Method method = null;
			
			try
			{
				method = object.getClass().getMethod(ReflectionUtil.getMethodName(fieldName, ReflectionUtil.GET));
			}
			catch(NoSuchMethodException e)
			{
				method = object.getClass().getMethod(ReflectionUtil.getMethodName(fieldName, ReflectionUtil.IS));
			}
			
			value = method.invoke(object);
		}
		catch(Exception e)
		{			
			throw new FieldIsNotAccessibleException(e);
		}
		
		return value;
	}
	
	/**
	 * Sets the field value.
	 * 
	 * @param object
	 * @param fieldName
	 * @param value
	 * @throws FieldIsNotAccessibleException 
	 */
	public static void setFieldValue(Object object, String fieldName, Object value) throws FieldIsNotAccessibleException
	{
		try
		{
			Method method = object.getClass().getMethod(ReflectionUtil.getMethodName(fieldName, ReflectionUtil.SET), value.getClass());
			
			method.invoke(object, value);
		}
		catch(Exception e)
		{
			throw new FieldIsNotAccessibleException(e);
		}
	}
	
	/**
	 * Returns all inherited declared fields. 
	 * 
	 * @param clazz
	 * @return List<Field>
	 */
	public static List<Field> getAllFields(Class<?> clazz)
	{
		List<Field> fieldList = new ArrayList<Field>();
		
		Class<?> clazzPointer = clazz;
		
		while(!clazzPointer.equals(Object.class))
		{
			for(Field field : clazzPointer.getDeclaredFields())
			{
				fieldList.add(field);
			}
			
			clazzPointer = clazzPointer.getSuperclass();
		}
		
		return fieldList;
	}
	
	/**
	 * Searches for an specific annotation.
	 * 
	 * @param annotations
	 * @param annotation
	 * @return Annotation
	 */
	public static <T extends Annotation> T findAnnotation(AnnotatedElement element, Class<T> annotation)
	{
		if(element.isAnnotationPresent(annotation))
		{
			return element.getAnnotation(annotation);
		}
		
		return null;
	}
	
	/**
	 * Checks if one of the specified annotations is present.
	 * 
	 * @param clazz
	 * @param annotation
	 * @return Annotation
	 */
	public static <T extends Annotation> boolean isAnnotationPresent(AnnotatedElement element, Class<?> ... annotations)
	{
		boolean result = false;
		
		Annotation[] classAnnotations = element.getAnnotations();
		
		if(classAnnotations == null || classAnnotations.length == 0)
		{
			classAnnotations = element.getDeclaredAnnotations();
		}
		
		if(classAnnotations != null && classAnnotations.length > 0)
		{
			for(Annotation classAnnotation : classAnnotations)
			{
				for(Class<?> annotation : annotations)
				{
					result |= classAnnotation.annotationType().equals(annotation);
				}
			}
		}
		
		return result;
	}
	
	/**
	 * Creates a new instance using string parameters.
	 * 
	 * @param clazz
	 * @param parameters
	 * @return T
	 * @throws UnsupportedTypeException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	public static <T> T createInstance(Class<T> clazz, String ... parameters) throws UnsupportedTypeException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		@SuppressWarnings("unchecked")
		Constructor<T> constructors[] = (Constructor<T>[])clazz.getConstructors();
		
		if(constructors == null || constructors.length == 0)
		{
			throw new RuntimeException("No public constructor was found.");
		}
		
		final int numParameters = parameters != null ? parameters.length : 0;
		
		T object = null;
		
		for(Constructor<T> constructor : constructors)
		{
			if(numParameters == constructor.getParameterTypes().length)
			{
				if(numParameters > 0)
				{
					Class<?> types[] = constructor.getParameterTypes();
					
					Object castedParameters[] = new Object[numParameters];
					
					for(int i=0; i<numParameters; i++)
					{
						castedParameters[i] = parseValue(types[i], parameters[i]);
					}
					
					object = constructor.newInstance(castedParameters);
				}
				else
				{
					object = constructor.newInstance();
				}
				
				break;
			}
		}
		
		return object;
	}
	
	/**
	 * Hidden.
	 */
	private ReflectionUtil()
	{
		;
	}
}
