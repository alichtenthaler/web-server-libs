package rnojiri.lib.util;

/**
 * Raised when a field is not accessible.
 * 
 * @author rnojiri
 */
public class FieldIsNotAccessibleException extends Exception
{
	private static final long serialVersionUID = 8947966925125567994L;

	public FieldIsNotAccessibleException(Throwable cause)
	{
		super(cause);
	}
	
}
