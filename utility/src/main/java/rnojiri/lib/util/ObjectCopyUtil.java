package rnojiri.lib.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Has useful functions to copy object properties.
 * 
 * @author rnojiri
 */
public class ObjectCopyUtil
{
	public static void copyNotEqualValues(Object origin, Object dest)
	{
		if(origin == null || dest == null) return;
		
		Class<?> originClass = origin.getClass();
		Class<?> destClass = dest.getClass();
		
		List<Field> originFields = ReflectionUtil.getAllFields(originClass);
		
		for(Field field : originFields)
		{
			Method setMethod = null;
			Class<?> destParameterType = null;
			Object originValue = null;
			
			try
			{
				Method originGetMethod = originClass.getMethod(ReflectionUtil.getMethodName(field.getName(), ReflectionUtil.GET));
				Method destGetMethod = destClass.getMethod(ReflectionUtil.getMethodName(field.getName(), ReflectionUtil.GET));
				
				originValue = originGetMethod.invoke(origin);
				Object destValue = destGetMethod.invoke(dest);
				
				if((originValue == null && destValue == null) || 
				   (originValue != null && destValue != null && origin.equals(destValue)))
				{
					continue;
				}
				
				destParameterType = destGetMethod.getReturnType();
			}
			catch(Exception e)
			{
				continue;
			}
			
			try
			{
				setMethod = destClass.getMethod(ReflectionUtil.getMethodName(field.getName(), ReflectionUtil.SET), destParameterType);
				
				setMethod.invoke(dest, originValue);
			}
			catch(Exception e)
			{
				continue;
			}
		}
	}
	
	private ObjectCopyUtil()
	{
		;
	}
}
