package rnojiri.lib.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Object serialization utilities.
 * 
 * @author rnojiri
 */
public class SerializationUtil
{
	/**
	 * Serializes the object to bytes.
	 * 
	 * @param obj
	 * @return byte[]
	 * @throws IOException
	 */
	public static byte[] serialize(Object obj) throws IOException
	{
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
		objectOutputStream.writeObject(obj);
		
		return byteArrayOutputStream.toByteArray();
	}
	
	/**
	 * Deserializes the bytes to an object.
	 * 
	 * @param data
	 * @param clazz
	 * @return T
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public static <T> T deserialize(byte[] data, Class<T> clazz) throws IOException, ClassNotFoundException
	{
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(data);
		ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
		
		return (T)objectInputStream.readObject();
	}
	
	/**
	 * Hidden.
	 */
	private SerializationUtil()
	{
		;
	}
}
