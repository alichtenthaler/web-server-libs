package rnojiri.lib.interfaces;

/**
 * Called when a exception is raised.
 * 
 * @author rnojiri
 */
public interface AsyncCallbackInterface
{
	/**
	 * Handles the success.
	 */
	void handleSuccess();
	
	/**
	 * Handles the exception.
	 * 
	 * @param e
	 */
	void handleException(Exception e);
}
