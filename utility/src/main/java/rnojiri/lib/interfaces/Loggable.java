package rnojiri.lib.interfaces;

import org.slf4j.Logger;

/**
 * A class with a logger.
 * 
 * @author rnojiri
 */
public interface Loggable
{
	/**
	 * Returns the class logger.
	 * 
	 * @return Logger
	 */
	Logger getLogger();
}
