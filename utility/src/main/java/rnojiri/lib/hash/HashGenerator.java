package rnojiri.lib.hash;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generates hashes.
 * 
 * @author rnojiri
 */
public class HashGenerator
{
	private static final String MD5 = "MD5";
	
	private static Logger log = LoggerFactory.getLogger(HashGenerator.class);
	
	/**
	 * Generates a new md5 using the given string.
	 * 
	 * @param string
	 * @return String
	 */
	public static String md5(String string)
	{
		if(string == null)
		{
			return null;
		}
		
		return md5(string.getBytes());
	}
	
	/**
	 * Generates a new md5 using the given the bytes.
	 * 
	 * @param bytes
	 * @return String
	 */
	public static String md5(byte[] bytes)
	{
		if(bytes == null)
		{
			return null;
		}
		
		try
		{
			MessageDigest md5MessageDigest = MessageDigest.getInstance(MD5);
			md5MessageDigest.update(bytes);

	        return new BigInteger(1, md5MessageDigest.digest()).toString(16);
		}
		catch(NoSuchAlgorithmException e)
		{
			log.error("MD5 algorithm was not found!");
			
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Hidden.
	 */
	private HashGenerator()
	{
		;
	}
}
