package rnojiri.lib.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A bean logger.
 * 
 * @author rnojiri
 */
public class BeanLogger
{
	/**
	 * Logs a bean creation.
	 * 
	 * @param beanClass
	 */
	public static void creating(Class<?> beanClass)
	{
		getLogger().info("Creating bean type \"{}\".", beanClass.getName());
	}
	
	/**
	 * Logs a bean creation by name.
	 * 
	 * @param beanName
	 */
	public static void creating(String beanName)
	{
		getLogger().info("Creating bean name \"{}\".", beanName);
	}
	
	/**
	 * Returns the current logger.
	 * 
	 * @return Logger
	 */
	private static Logger getLogger()
	{
		StackTraceElement[] array = Thread.currentThread().getStackTrace();
		
		try
		{
			return LoggerFactory.getLogger(Class.forName(array[3].getClassName()));
		}
		catch (ClassNotFoundException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	private BeanLogger()
	{
		;
	}
}
