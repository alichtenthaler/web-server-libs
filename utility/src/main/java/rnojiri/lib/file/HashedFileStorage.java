package rnojiri.lib.file;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.bouncycastle.crypto.digests.MD5Digest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rnojiri.lib.support.Constants;

/**
 * Implements a hashed file storage.
 * 
 * @author rnojiri
 */
public class HashedFileStorage
{
	private static final int NUM_MD5_BYTES = 16;

	private static final char[] HEX_CHARS = "0123456789ABCDEF".toCharArray();

	private static final Logger LOGGER = LoggerFactory.getLogger(HashedFileStorage.class);

	private final Path baseDirectoryPath;
	
	private final int numPathDepth;
	
	private final int numBytesPerDirectory;
	
	private final int numBaseDirectoryPathChars;
	
	/**
	 * @param baseDirectory
	 * @param numPathDepth
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidDepthException
	 */
	public HashedFileStorage(final String baseDirectory, final int numPathDepth) throws IOException, NoSuchAlgorithmException, InvalidDepthException
	{
		if(numPathDepth <= 0 || (numPathDepth > 1 && numPathDepth % 2 != 0))
		{
			throw new InvalidDepthException(numPathDepth, NUM_MD5_BYTES);
		}
		
		this.baseDirectoryPath = Paths.get(baseDirectory);
		this.numPathDepth = numPathDepth;
		this.numBytesPerDirectory = NUM_MD5_BYTES / numPathDepth;
		this.numBaseDirectoryPathChars = (baseDirectory.charAt(baseDirectory.length() - 1) == '/' ? baseDirectory.length() - 1 : baseDirectory.length()) ;

		if (!Files.exists(baseDirectoryPath))
		{
			LOGGER.info("Creating directory {}...", baseDirectoryPath);

			Files.createDirectories(baseDirectoryPath);
		}

		LOGGER.info("Using directory {} for file storage.", baseDirectoryPath);
	}

	/**
	 * Saves the file on the path and returns the number of bytes saved.
	 * 
	 * @param name
	 * @param inputStream
	 * @return long
	 * @throws IOException 
	 */
	public FileOpDescriptor save(final String name, final FileProcessedData processedData) throws IOException
	{
		final Path m5dPath = buildMd5Path(processedData.md5sum);
		
		Files.createDirectories(m5dPath);
		
		Path filePath = m5dPath.resolve(name);
		
		Files.write(filePath, processedData.bytes, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
		
		LOGGER.info("Image {} ({} bytes) was saved on path {}.", name, processedData.bytes.length, filePath);
		
		return new FileOpDescriptor(filePath, processedData.bytes, processedData.md5sum);
	}
	
	/**
	 *  Builds a new path using the specified input stream.
	 * 
	 * @param name
	 * @param inputStream
	 * @return Path
	 * @throws Exception
	 */
	public FileProcessedData processInputStream(InputStream inputStream) throws IOException
	{
		MD5Digest md5Digest = new MD5Digest();		
		int buffer = 0;
		List<Byte> byteList = new ArrayList<>();
		
		while ((buffer = inputStream.read()) != -1)
		{
			byte b = (byte)buffer;
			
			byteList.add(b);
			md5Digest.update(b);
		}
		
		byte[] digestResult = new byte[md5Digest.getDigestSize()];
		
		md5Digest.doFinal(digestResult, 0);
		
		byte bytes[] = new byte[byteList.size()];
		
		for(int i=0; i<bytes.length; i++)
		{
			bytes[i] = byteList.get(i);
		}
		
		return new FileProcessedData(bytes, digestResult);
	}
	
	/**
	 * Builds the md5sum path.
	 * 
	 * @param md5sum
	 * @return Path
	 */
	protected Path buildMd5Path(byte[] md5sum)
	{
		String pathArray[] = new String[numPathDepth];
		
		StringBuilder builder = new StringBuilder(NUM_MD5_BYTES);
		
		int md5Index=0;
		
		for(int i=0; i<numPathDepth; i++)
		{
			for(int j=0; j<numBytesPerDirectory; j++, md5Index++)
			{
				builder.append(toHex(md5sum[md5Index]));
			}
			
			pathArray[i] = builder.toString();
			builder.delete(0, builder.length());
		}
		
		return baseDirectoryPath.resolve(Paths.get(Constants.EMPTY, pathArray));
	}
	
	/**
	 * Returns the value in hex.
	 * 
	 * @param value
	 * @return String
	 */
	protected String toHex(byte value)
	{
		char[] hexChars = new char[2];
		
		int v = value & 0xFF;
		hexChars[0] = HEX_CHARS[v >>> 4];
		hexChars[1] = HEX_CHARS[v & 0x0F];
		
		return new String(hexChars);
	}

	/**
	 * @return the numBaseDirectoryPathChars
	 */
	public int getNumBaseDirectoryPathChars()
	{
		return numBaseDirectoryPathChars;
	}
}
