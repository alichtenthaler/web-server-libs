package rnojiri.lib.validation.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A regular expression validation.
 * 
 * @author rnojiri
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RegexpValidation
{
	/**
	 * The regular expression.
	 * 
	 * @return String
	 */
	String value();
}
