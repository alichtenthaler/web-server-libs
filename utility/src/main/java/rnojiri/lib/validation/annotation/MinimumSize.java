package rnojiri.lib.validation.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * A field with minimum size.
 * 
 * @author rnojiri
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface MinimumSize
{
	/**
	 * The parameter size value.
	 * 
	 * @return int
	 */
	int value();
}
