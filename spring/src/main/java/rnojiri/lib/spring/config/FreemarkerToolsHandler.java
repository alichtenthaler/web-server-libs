package rnojiri.lib.spring.config;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import rnojiri.lib.freemarker.tool.FreemarkerTool;
import rnojiri.lib.freemarker.tool.factory.CollectionToolFactory;
import rnojiri.lib.freemarker.tool.factory.ObjectToolFactory;
import rnojiri.lib.freemarker.tool.factory.RequestVariablesToolFactory;
import rnojiri.lib.freemarker.tool.factory.ToolFactory;

/**
 * Put all tools configured in the ModelAndView object.
 * 
 * @author rnojiri
 */
public class FreemarkerToolsHandler extends HandlerInterceptorAdapter
{
	private List<ToolFactory<?>> toolFactoryList;
	
	public FreemarkerToolsHandler()
	{
		toolFactoryList = new ArrayList<>();
		toolFactoryList.add(new CollectionToolFactory());
		toolFactoryList.add(new ObjectToolFactory());
		toolFactoryList.add(new RequestVariablesToolFactory());
	}
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception
	{
		super.postHandle(request, response, handler, modelAndView);
		
		if(toolFactoryList != null && modelAndView != null)
		{
			for(ToolFactory<?> toolFactory : toolFactoryList)
			{
				FreemarkerTool tool = toolFactory.getInstance(request);
				
				modelAndView.addObject(tool.getName(), tool);
			}
		}
	}
}
