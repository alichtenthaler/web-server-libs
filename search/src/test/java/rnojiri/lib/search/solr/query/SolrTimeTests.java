package rnojiri.lib.search.solr.query;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Testes da classe SolrTimeTests
 * 
 * @author rnojiri
 */
public class SolrTimeTests {

	@Test
	public void testDefaultConstructor() {
		
		SolrTime solrTime = new SolrTime();
		
		assertEquals("NOW", solrTime.getValue());
	}
	
	@Test
	public void testConstructorWithNOW() {
		
		SolrTime solrTime = new SolrTime(1, SolrTime.Measure.NOW);
		
		assertEquals("NOW", solrTime.getValue());
		
		solrTime = new SolrTime(-1, SolrTime.Measure.NOW);
		
		assertEquals("NOW", solrTime.getValue());
	}
	
	@Test
	public void testConstructorWithDAY() {
		
		SolrTime solrTime = new SolrTime(1, SolrTime.Measure.DAY);
		
		assertEquals("+1DAY", solrTime.getValue());
		
		solrTime = new SolrTime(-1, SolrTime.Measure.DAY);
		
		assertEquals("-1DAY", solrTime.getValue());
	}
	
	@Test
	public void testConstructorWithMONTH() {
		
		SolrTime solrTime = new SolrTime(1, SolrTime.Measure.MONTH);
		
		assertEquals("+1MONTH", solrTime.getValue());
		
		solrTime = new SolrTime(-1, SolrTime.Measure.MONTH);
		
		assertEquals("-1MONTH", solrTime.getValue());
	}
	
	@Test
	public void testConstructorWithYEAR() {
		
		SolrTime solrTime = new SolrTime(1, SolrTime.Measure.YEAR);
		
		assertEquals("+1YEAR", solrTime.getValue());
		
		solrTime = new SolrTime(-1, SolrTime.Measure.YEAR);
		
		assertEquals("-1YEAR", solrTime.getValue());
	}
	
	@Test
	public void testSetNOW() {
		
		SolrTime solrTime = new SolrTime();
		
		solrTime.setNow();
		
		assertEquals("NOW", solrTime.getValue());
		
		solrTime.setValue(100, SolrTime.Measure.NOW);
		
		assertEquals("NOW", solrTime.getValue());
	}
	
	@Test
	public void testAddNOW() {
		
		SolrTime solrTime = new SolrTime();
		
		solrTime.setNow();
		
		assertEquals("NOW", solrTime.getValue());
		
		solrTime.addValue(100, SolrTime.Measure.NOW);
		
		assertEquals("NOW", solrTime.getValue());
	}
	
	@Test
	public void testAddDAY() {
		
		SolrTime solrTime = new SolrTime().addValue(1, SolrTime.Measure.DAY);
		
		assertEquals("NOW+1DAY", solrTime.getValue());
		
		solrTime.addValue(-1, SolrTime.Measure.DAY);
		
		assertEquals("NOW+1DAY-1DAY", solrTime.getValue());
	}
	
	@Test
	public void testAddMONTH() {
		
		SolrTime solrTime = new SolrTime().addValue(1, SolrTime.Measure.MONTH);
		
		assertEquals("NOW+1MONTH", solrTime.getValue());
		
		solrTime.addValue(-1, SolrTime.Measure.MONTH);
		
		assertEquals("NOW+1MONTH-1MONTH", solrTime.getValue());
	}
	
	@Test
	public void testAddYEAR() {
		
		SolrTime solrTime = new SolrTime().addValue(1, SolrTime.Measure.YEAR);
		
		assertEquals("NOW+1YEAR", solrTime.getValue());
		
		solrTime.addValue(-1, SolrTime.Measure.YEAR);
		
		assertEquals("NOW+1YEAR-1YEAR", solrTime.getValue());
	}
	
	@Test
	public void testRoundNOW() {
		
		SolrTime solrTime = new SolrTime();
		
		solrTime.setNow();
		
		assertEquals("NOW", solrTime.getValue());
		
		solrTime.round(SolrTime.Measure.NOW);
		
		assertEquals("NOW", solrTime.getValue());
	}
	
	@Test
	public void testRoundDAY() {
		
		SolrTime solrTime = new SolrTime();
		
		solrTime.round(SolrTime.Measure.DAY);
		
		assertEquals("NOW/DAY", solrTime.getValue());
	}
	
	@Test
	public void testRoundMONTH() {
		
		SolrTime solrTime = new SolrTime();
		
		solrTime.round(SolrTime.Measure.MONTH);
		
		assertEquals("NOW/MONTH", solrTime.getValue());
	}
	
	@Test
	public void testRoundYEAR() {
		
		SolrTime solrTime = new SolrTime();
		
		solrTime.round(SolrTime.Measure.YEAR);
		
		assertEquals("NOW/YEAR", solrTime.getValue());
	}
}
