package rnojiri.lib.search.filter;

import java.io.Serializable;

/**
 * Filter properties.
 * 
 * @author rnojiri
 */
public class FilterProperty implements Serializable
{
	private static final long serialVersionUID = 9199628579782115545L;

	private String id;

	private String value;

	private Long count;

	private boolean checked;

	private boolean enabled;

	private boolean visible;

	/**
	 * @param id
	 * @param value
	 * @param checked
	 * @param enabled
	 */
	public FilterProperty(final String id, final String value, final Long count, final boolean checked, final boolean enabled)
	{
		this.id = id;
		this.value = value;
		this.count = count;
		this.checked = checked;
		this.enabled = enabled;
		this.visible = true;
	}

	/**
	 * @param id
	 * @param value
	 * @param checked
	 * @param enabled
	 */
	public FilterProperty(final String id, final String value, final Long count, final boolean checked, final boolean enabled, final boolean visible)
	{
		this.id = id;
		this.value = value;
		this.count = count;
		this.checked = checked;
		this.enabled = enabled;
		this.visible = visible;
	}

	/**
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}
	
	/**
	 * @param id
	 */
	public void setId(String id)
	{
		this.id = id;
	}

	/**
	 * @return the value
	 */
	public String getValue()
	{
		return value;
	}
	
	/**
	 * @param value
	 */
	public void setValue(String value)
	{
		this.value = value;
	}

	/**
	 * @return the count
	 */
	public Long getCount()
	{
		return count;
	}

	/**
	 * @param count
	 *            the count to set
	 */
	public void setCount(Long count)
	{
		this.count = count;
	}

	/**
	 * Adds to count.
	 */
	public void addToCount()
	{
		this.count++;
	}

	/**
	 * @return the checked
	 */
	public boolean isChecked()
	{
		return checked;
	}

	/**
	 * @param checked
	 *            the checked to set
	 */
	public void setChecked(boolean checked)
	{
		this.checked = checked;
	}

	/**
	 * @return the enabled
	 */
	public boolean isEnabled()
	{
		return enabled;
	}

	/**
	 * @param enabled
	 *            the enabled to set
	 */
	public void setEnabled(boolean enabled)
	{
		this.enabled = enabled;
	}

	/**
	 * @return the visible
	 */
	public boolean isVisble()
	{
		return visible;
	}

	/**
	 * @param visible
	 *            the visible to set
	 */
	public void setVisble(boolean visible)
	{
		this.visible = visible;
	}
}
