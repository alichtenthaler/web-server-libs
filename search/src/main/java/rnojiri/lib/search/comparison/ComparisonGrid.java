package rnojiri.lib.search.comparison;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * A generic comparison grid.
 * 
 * @author rnojiri
 */
public class ComparisonGrid<T>
{
	private Map<String, ComparisonItem<T>[]> gridMap;
	
	private final int width;
	
	private final boolean inverseWeightComparison;
	
	/**
	 * @param width
	 * @param inverseWeightComparison
	 */
	public ComparisonGrid(final int width)
	{
		this(width, false);
	}
	
	/**
	 * @param width
	 * @param inverseWeightComparison
	 */
	public ComparisonGrid(final int width, final boolean inverseWeightComparison)
	{
		this.width = width;
		this.inverseWeightComparison = inverseWeightComparison;
		
		gridMap = new LinkedHashMap<String, ComparisonItem<T>[]>();
	}
	
	/**
	 * Adds a new comparison item to the grid line.
	 * 
	 * @param key
	 * @param item
	 * @param column
	 */
	@SuppressWarnings("unchecked")
	public void put(final String key, final ComparisonItem<T> item, int column)
	{
		if(column >= width)
		{
			throw new IndexOutOfBoundsException("The maximum width (" + width + ") from the grid was exceeded.");
		}
		
		ComparisonItem<T>[] comparisonItems = gridMap.get(key);
		
		if(comparisonItems == null)
		{
			comparisonItems = (ComparisonItem<T>[]) new ComparisonItem[width];
			
			gridMap.put(key, comparisonItems);
		}
		
		comparisonItems[column] = item;
	}
	
	/**
	 * Compute all comparison items results.
	 */
	private void computeComparisonResults()
	{
		for(ComparisonItem<T>[] value : gridMap.values())
		{
			int bestIndex = -1;
			Float lastWeight = null;
			boolean equalsWeight = false;
			
			for(int i=0; i<width; i++)
			{
				if(value[i] == null) continue;
				
				Float currentWeight = value[i].getWeight();
				
				if(currentWeight != null)
				{
					if(lastWeight != null)
					{
						if(lastWeight.equals(currentWeight))
						{
							equalsWeight = true;
						}
						else if((!inverseWeightComparison && lastWeight.floatValue() < currentWeight.floatValue()) || 
								 (inverseWeightComparison && lastWeight.floatValue() > currentWeight.floatValue()))
						{
							lastWeight = currentWeight;
							bestIndex = i;
							equalsWeight = false;
						}
					}
					else
					{
						lastWeight = currentWeight;
						bestIndex = i;
					}
				}
			}
			
			if(!equalsWeight && bestIndex != -1)
			{
				value[bestIndex].setTheBestChoice(true);
			}
		}
	}
	
	/**
	 * Returns comparison grid result reader.
	 * 
	 * @return ComparisonGridResultReader
	 */
	public ComparisonGridResultReader<T> getComparisonGridResultReader()
	{
		computeComparisonResults();
		
		return new ComparisonGridResultReader<T>(gridMap, width);
	}
	
	/**
	 * Returns the number of lines.
	 * 
	 * @return int
	 */
	public int getNumLines()
	{
		return gridMap.size();
	}
	
	/**
	 * Returns the number of columns.
	 * 
	 * @return int
	 */
	public int getNumColumns()
	{
		return width;
	}
}