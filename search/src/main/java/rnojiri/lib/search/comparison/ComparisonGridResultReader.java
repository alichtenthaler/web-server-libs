package rnojiri.lib.search.comparison;

import java.util.Map;

/**
 * The comparison grid line iterator.
 * 
 * @author rnojiri
 */
public class ComparisonGridResultReader<T>
{
	private String keys[];
	
	private ComparisonItem<T>[] values[];
	
	private int numLines;
	
	private int numColumns;
	
	private boolean empty;
	
	/**
	 * Sets the grid map.
	 * 
	 * @param gridMap
	 * @param numColumns
	 */
	@SuppressWarnings("unchecked")
	public ComparisonGridResultReader(final Map<String, ComparisonItem<T>[]> gridMap, final int numColumns)
	{
		numLines = gridMap.size();
		this.numColumns = numColumns;
		
		if(numLines == 0 && numColumns == 0)
		{
			empty = true;
			
			return;
		}
		
		keys = new String[numLines];
		values = (ComparisonItem<T>[][]) new ComparisonItem[numLines][];
		
		int i=0;
		
		for(String key : gridMap.keySet())
		{
			keys[i++] = key;
		}
		
		i=0;
		
		for(ComparisonItem<T>[] value : gridMap.values())
		{
			values[i++] = value;
		}
	}
	
	/**
	 * Returns the line key.
	 * 
	 * @param line
	 * @return String
	 */
	public String getKey(int line)
	{
		return keys[line];
	}
	
	/**
	 * Returns the item description.
	 * 
	 * @param line
	 * @param column
	 * @return String
	 */
	public T getValue(int line, int column)
	{
		return values[line][column] != null ? values[line][column].getValue() : null;
	}
	
	public String getOptional(int line, int column)
	{
		return values[line][column] != null ? values[line][column].getOptional() : null;
	}
	
	/**
	 * Returns if the item is the best choice.
	 * 
	 * @param line
	 * @param column
	 * @return String
	 */
	public boolean isTheBestChoice(int line, int column)
	{
		return values[line][column] != null ? values[line][column].isTheBestChoice() : false;
	}

	/**
	 * @return the numLines
	 */
	public int getNumLines()
	{
		return numLines;
	}
	
	/**
	 * @return the numColumns
	 */
	public int getNumColumns()
	{
		return numColumns;
	}

	/**
	 * @return the empty
	 */
	public boolean isEmpty()
	{
		return empty;
	}
}
