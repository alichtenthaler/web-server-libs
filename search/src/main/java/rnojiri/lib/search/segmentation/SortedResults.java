package rnojiri.lib.search.segmentation;

import java.util.List;

import org.apache.solr.client.solrj.response.QueryResponse;

/**
 * The sorted results from a query search.
 * 
 * @author rnojiri
 *
 * @param <T>
 */
public class SortedResults<T>
{
	private final List<T> sortedResults;
	
	private final QueryResponse queryResponse;

	/**
	 * @param sortedResults
	 * @param queryResponse
	 */
	public SortedResults(List<T> sortedResults, QueryResponse queryResponse)
	{
		this.sortedResults = sortedResults;
		this.queryResponse = queryResponse;
	}

	/**
	 * @return the sortedResults
	 */
	public List<T> getSortedResults()
	{
		return sortedResults;
	}

	/**
	 * @return the queryResponse
	 */
	public QueryResponse getQueryResponse()
	{
		return queryResponse;
	}
}
