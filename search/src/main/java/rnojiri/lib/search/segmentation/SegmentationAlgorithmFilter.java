package rnojiri.lib.search.segmentation;

import java.util.List;

import rnojiri.lib.search.solr.service.AbstractSearchParameters;

/**
 * A segment Algorirthm Filter filter.
 * 
 * 
 * First define/mark what items belong to the segment with the method isElegible . Second define your algorithm for compare items
 * 
 * @author rnojiri
 * 
 * @param <T>  Model that you want to ordered
 * @param <P>  Parameters that will be added for filter
 */
public interface SegmentationAlgorithmFilter<T, P extends AbstractSearchParameters>
{
	/**
	 * Tests if an item is eligible to the segment.
	 * 
	 * @param item
	 * @param parameters
	 * @return boolean
	 */
	boolean isEligible(T item, P parameters);
	
	/**
	 * Apply a post operation(could be a Order ) for the segmented list.
	 * 
	 * @param segmentedList
	 * @return List<T>
	 */
	List<T> postOperation(List<T> segmentedList);
}
