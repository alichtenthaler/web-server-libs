package rnojiri.lib.search.solr.service;

import java.io.Serializable;
import java.util.List;

import rnojiri.lib.search.filter.Filter;

/**
 * Search results.
 * 
 * @author rnojiri
 */
public class SearchResults<T> implements Serializable
{
	private static final long serialVersionUID = -5838982309306023041L;

	protected final List<T> results;
	
	protected final List<Filter> filters;
	
	/**
	 * @param results
	 */
	public SearchResults(final List<T> results)
	{
		this(results, null);
	}

	/**
	 * @param filters
	 */
	public SearchResults(final List<T> results, final List<Filter> filters)
	{
		this.results = results;
		this.filters = filters;
	}

	/**
	 * @return the results
	 */
	public List<T> getResults()
	{
		return results;
	}

	/**
	 * @return the filters
	 */
	public List<Filter> getFilters()
	{
		return filters;
	}
}
