package rnojiri.base.servlet.controller.exception;

/**
 * Raised when a user already exists on the database.
 * 
 * @author rnojiri
 */
public class UserAlreadyExistsException extends Exception
{
	private static final long serialVersionUID = 3743015571005304485L;

	/**
	 * @param userId
	 */
	public UserAlreadyExistsException(String userId)
	{
		super("User \"" + userId + "\" already exists.");
	}
	
}
