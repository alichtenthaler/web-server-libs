package rnojiri.base.servlet.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpHeaders;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import rnojiri.base.servlet.controller.form.SignInForm;
import rnojiri.lib.interfaces.Loggable;
import rnojiri.lib.shiro.service.IUserLoader;
import rnojiri.lib.support.CommonCodeResults;
import rnojiri.lib.support.JsonResult;

/**
 * The standard sign in controller.
 * 
 * @author rnojiri
 */
@Component
public abstract class AbstractSignInController<F extends SignInForm, I, U, J> extends AbstractSignInOrRegistrationController<I, U, J> implements Loggable
{	
	@Autowired
	protected IUserLoader<I, String> userLoader;
	
	/**
	 * Provides the sign in page.
	 * 
	 * @return ModelAndView
	 */
	public ModelAndView signInPage(Subject user, HttpServletRequest request)
	{
		ModelAndView redirect = redirIfHasUser(user, request);
		
		return (redirect != null) ? redirect : getModelAndView();
	}

	/**
	 * Redirects the user if he is already logged.
	 * 
	 * @param user
	 * @param request
	 * @return ModelAndView
	 */
	private ModelAndView redirIfHasUser(Subject user, HttpServletRequest request)
	{
		if(user.isAuthenticated())
		{
			return urlManager.getRedirectModelAndView(request.getHeader(HttpHeaders.REFERER), user.getSession());
		}
		
		return null;
	}
	
	/**
	 * Does the authentication.
	 * 
	 * @param form
	 * @param userSubject
	 * @return String
	 * @throws Exception
	 */
	private String authenticate(F form, Subject userSubject) throws Exception
	{
		UsernamePasswordToken token = new UsernamePasswordToken(form.getEmail(), form.getPassword());
		
		userSubject.login(token);
		
		return (String)userSubject.getPrincipal();
	}
	
	/**
	 * Sends the sign in information.
	 * 
	 * @param form
	 * @param userSubject
	 * @param request
	 * @return ModelAndView
	 */
	public ModelAndView signInAction(F form, Subject userSubject, HttpServletRequest request)
	{
		ModelAndView redirect = redirIfHasUser(userSubject, request);
		
		if(redirect != null) return redirect;
		
		ModelAndView modelAndView = getModelAndView();
		
		if(!isValid(modelAndView, form))
		{
			return modelAndView;
		}
		
		try
		{			
			Session session = userSubject.getSession();
			
			authenticate(form, userSubject);
			
			return urlManager.getRedirectModelAndView(form.getRedirect(), session);
		}
		catch(IncorrectCredentialsException e)
		{
			addGenericError(modelAndView, CommonCodeResults.NOT_AUTHORIZED);
			
			getLogger().debug("Incorrect credentials sent for user {}.", form.getEmail(), e);
		}
		catch (UnknownAccountException e) 
		{
			getLogger().debug("No account {} was found.", form.getEmail(), e);
			
			addGenericError(modelAndView, CommonCodeResults.NOT_AUTHORIZED);
		}
		catch(Exception e)
		{
			addGenericError(modelAndView, CommonCodeResults.FAILURE);
			
			getLogger().error("Error authenticating user.", e);
		}
		
		return modelAndView;
	}
	
	/**
	 * Sends the sign in information.
	 * 
	 * @param form
	 * @param userSubject
	 * @param response
	 * @return ResultJson<String, Object>
	 */
	@SuppressWarnings("unchecked")
	public JsonResult<String, Object> signInApi(F form, Subject userSubject, HttpServletResponse response)
	{
		JsonResult<String, Object> jsonResult = new JsonResult<>();
		
		if(!isValid(jsonResult, response, form)) return jsonResult;
		
		if(!userSubject.isAuthenticated())
		{
			try
			{			
				String externalId = authenticate(form, userSubject);
				
				addSucess(jsonResult, toJson((U)userLoader.loadUserByExternalId(externalId)));
			}
			catch(IncorrectCredentialsException e)
			{
				getLogger().debug("Incorrect credentials sent for user {}.", form.getEmail(), e);
				
				addGenericResult(jsonResult, CommonCodeResults.NOT_AUTHORIZED, HttpServletResponse.SC_FORBIDDEN, response);
			}
			catch (UnknownAccountException e) 
			{
				getLogger().debug("No account {} was found.", form.getEmail(), e);
				
				addGenericResult(jsonResult, CommonCodeResults.NOT_AUTHORIZED, HttpServletResponse.SC_FORBIDDEN, response);
			}
			catch(AuthenticationException e)
			{
				getLogger().debug(e.getMessage());
				
				addGenericResult(jsonResult, CommonCodeResults.NOT_AUTHORIZED, HttpServletResponse.SC_FORBIDDEN, response);
			}
			catch(Exception e)
			{
				getLogger().error("Error authenticating user: {}", form.getEmail(), e);
				
				addGenericError(jsonResult, CommonCodeResults.FAILURE, response);
			}
		}
		else
		{
			if(getLogger().isDebugEnabled())
			{
				getLogger().debug("User {} is already authenticated.", form.getEmail());
			}
			
			addSucess(jsonResult, toJson(getUser()));
		}
		
		return jsonResult;
	}
	
	/**
	 * Signs out.
	 * 
	 * @param redir
	 * @param userSubject
	 * @param response
	 */
	public void signOutAction(String redir, Subject userSubject, HttpServletResponse response)
	{
		try
		{
			userSubject.logout();
			
			response.sendRedirect(urlManager.getRedirectUrl(redir, userSubject.getSession()));
		}
		catch(Exception e)
		{
			getLogger().error("Error signing out.", e);
		}
	}
	
	/**
	 * Signs out using api.
	 * 
	 * @param userSubject
	 * @param response
	 * @return JsonResult<String, Object>
	 */
	public JsonResult<String, Object> signOutApi(Subject userSubject, HttpServletResponse response)
	{
		JsonResult<String, Object> jsonResult = new JsonResult<>();
		
		try
		{
			userSubject.logout();
			
			addSucess(jsonResult);
		}
		catch(Exception e)
		{
			getLogger().error("Error signing out from API.", e);
			
			addGenericError(jsonResult, CommonCodeResults.FAILURE, response);
		}
		
		return jsonResult;
	}
}
