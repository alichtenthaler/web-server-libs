package rnojiri.base.servlet.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authc.credential.PasswordService;
import org.apache.shiro.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import rnojiri.base.servlet.controller.exception.UserAlreadyExistsException;
import rnojiri.base.servlet.controller.form.RegistrationForm;
import rnojiri.lib.support.CommonCodeResults;
import rnojiri.lib.support.JsonResult;

/**
 * Registration controller. 
 * 
 * @author rnojiri
 */
@Component
public abstract class AbstractRegistrationController<F extends RegistrationForm, I, U, J> extends AbstractSignInOrRegistrationController<I, U, J>
{
	@Autowired
	private PasswordService passwordService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRegistrationController.class);
	
	/**
	 * Provides the login page.
	 * 
	 * @return ModelAndView
	 */
	public ModelAndView registerPage()
	{
		return getModelAndView();
	}
	
	/**
	 * Saves the user in database.
	 * 
	 * @param form
	 * @param encodedPassword
	 * @return <I>
	 * @throws Exception
	 */
	protected abstract I saveUserInDatabase(F form, final String encodedPassword) throws Exception;
	
	/**
	 * Register the user.
	 * 
	 * @return ModelAndView
	 */
	public ModelAndView registerAction(F form, Session session)
	{
		ModelAndView modelAndView = getModelAndView();
		
		if(!isValid(modelAndView, form))
		{
			return modelAndView;
		}
		
		try
		{
			I userId = saveUserInDatabase(form, passwordService.encryptPassword(form.getPassword()));
			
			U user = getUserFromDatabase(userId);
			
			String redirect = urlManager.getRedirectUrl(form.getRedirect(), session);
			
			modelAndView.addObject(MATTR_USER, user);
			modelAndView.addObject(MATTR_REDIRECT, redirect);
			
			return urlManager.getRedirectModelAndView(form.getRedirect(), session);
		}
		catch(UserAlreadyExistsException e)
		{
			LOGGER.error(e.getMessage(), e);
			
			addGenericError(modelAndView, CommonCodeResults.DUPLICATED);
			
			return modelAndView;
		}
		catch(Exception e)
		{
			LOGGER.error("Error inserting a new user.", e);
			
			addGenericError(modelAndView, CommonCodeResults.FAILURE);
			
			return modelAndView;
		}
	}
	
	/**
	 * Register via web service.
	 * 
	 * @param form
	 * @param session
	 * @param response
	 * @return ResultJson<String, SigninOrRegistrationResultJsonData>
	 */
	public JsonResult<String, Object> registerApi(F form, Session session, HttpServletResponse response)
	{
		JsonResult<String, Object> jsonResult = new JsonResult<>();
		
		if(!isValid(jsonResult, response, form)) return jsonResult;
		
		try
		{
			I userId = saveUserInDatabase(form, passwordService.encryptPassword(form.getPassword()));
			
			U user = getUserFromDatabase(userId);
			
			J json = toJson(user);
			
			addSucess(jsonResult, json);
		}
		catch(UserAlreadyExistsException e)
		{
			LOGGER.error(e.getMessage(), e);
			
			addGenericResult(jsonResult, CommonCodeResults.DUPLICATED, HttpServletResponse.SC_CONFLICT, response);
		}
		catch(Exception e)
		{
			LOGGER.error("Error inserting a new user via WS", e);
			
			addGenericError(jsonResult, CommonCodeResults.FAILURE, response);
		}
		
		return jsonResult;
	}
}
