package rnojiri.base.servlet.controller.form;

import rnojiri.lib.validation.annotation.RegexpValidation;
import rnojiri.lib.validation.annotation.RequiredParam;

/**
 * E-mail field with validation.
 * 
 * @author rnojiri
 */
public interface EmailField
{
	/**
	 * Returns the e-mail.
	 * 
	 * @return String
	 */
	@RequiredParam
	@RegexpValidation("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
	String getEmail();
}
