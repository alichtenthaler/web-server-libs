package rnojiri.base.servlet.controller.form;

import rnojiri.lib.validation.annotation.RegexpValidation;
import rnojiri.lib.validation.annotation.RequiredParam;

/**
 * Has a simple name field with validation.
 * 
 * @author rnojiri
 */
public interface SimpleNameField
{
	/**
	 * Returns the simple name.
	 * 
	 * @return String
	 */
	@RequiredParam
	@RegexpValidation("^[\\p{IsLatin}\\'\\.0-9]{1}[\\p{IsLatin}-\\'\\.0-9 ]*$")
	String getName();
}
