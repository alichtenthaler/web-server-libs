package rnojiri.base.servlet;

import java.io.IOException;

import rnojiri.lib.shiro.jetty.ShiroEmbeddedJetty;
import rnojiri.lib.util.PropertiesReader;

/**
 * Base embedded jetty servlet implementation.
 * 
 * @author rnojiri
 */
public class BaseEmbeddedJettyServlet extends ShiroEmbeddedJetty
{
	/**
	 * @param properties
	 * @throws IOException
	 */
	public BaseEmbeddedJettyServlet(PropertiesReader properties) throws IOException
	{
		super(properties);
	}
}
