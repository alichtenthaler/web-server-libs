package rnojiri.jetty;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import rnojiri.lib.util.PropertiesManager;
import rnojiri.lib.util.PropertiesReader;

/**
 * The main class.
 * 
 * @author rnojiri
 */
public class Main
{
	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		getEmbeddedJettyImpl(args);
	}

	/**
	 * Returns a new instance of AbstractEmbeddedJetty implementation.
	 * 
	 * @param args
	 * @return AbstractEmbeddedJetty
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IOException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 */
	private static AbstractEmbeddedJetty getEmbeddedJettyImpl(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException
	{
		PropertiesReader properties = loadProperties(args);
		
		String embeddedJettyImplClass = properties.getProperty("embedded.jetty.implementation.class");
		
		if (embeddedJettyImplClass != null)
		{
			Class<?> clazz = Class.forName(embeddedJettyImplClass);
			Constructor<?> constructor = clazz.getConstructor(PropertiesReader.class);
			
			return (AbstractEmbeddedJetty)constructor.newInstance(properties);
		}

		throw new ClassNotFoundException("Embedded jetty implementation was not specified.");
	}
	
	/**
	 * Load the properties file.
	 * 
	 * @param args
	 * @return PropertiesReader
	 * @throws IOException
	 */
	private static PropertiesReader loadProperties(String[] args) throws IOException
	{
		String configurationPath = null;
		
		if(args == null || args.length == 0)
		{
			configurationPath = "jetty.properties";
		}
		else
		{
			configurationPath = args[0];
		}
		
		return PropertiesManager.getInstance().loadProperties(configurationPath, false);
	}
}
