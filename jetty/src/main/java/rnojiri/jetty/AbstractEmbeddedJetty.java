package rnojiri.jetty;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpServlet;

import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.RequestLogHandler;
import org.eclipse.jetty.server.session.AbstractSessionDataStore;
import org.eclipse.jetty.server.session.DefaultSessionCache;
import org.eclipse.jetty.server.session.NullSessionCacheFactory;
import org.eclipse.jetty.server.session.SessionCache;
import org.eclipse.jetty.server.session.SessionData;
import org.eclipse.jetty.server.session.SessionDataStore;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.BlockingArrayQueue;
import org.eclipse.jetty.util.log.Slf4jLog;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.slf4j.Logger;

import rnojiri.jetty.session.CustomSessionDataStore;
import rnojiri.jetty.session.MemcachedDataStore;
import rnojiri.lib.cache.CacheInstance;
import rnojiri.lib.util.PropertiesReader;
import rnojiri.lib.util.ReflectionUtil;
import rnojiri.lib.util.UnsupportedTypeException;

/**
 * Defines a common configuration for the embedded jetty.
 * 
 * @author rnojiri
 */
public abstract class AbstractEmbeddedJetty
{
	private static final String CONTEXT_PATH = "/";
	private static final String MAPPING_URL = "/*";
	
	protected PropertiesReader properties;
	
	protected CacheInstance cacheInstance;
	
	private Server server;
	
	private Logger logger;

	/**
	 * Configures the new instance.
	 * 
	 * @param propertiesFile
	 * @throws RuntimeException
	 */
	public AbstractEmbeddedJetty(PropertiesReader properties) 
	{
		this.properties = properties;
		
		try
		{
			org.eclipse.jetty.util.log.Log.setLog(createSlf4jLog());
			
			this.logger = getLogger();
			
			if(properties == null)
			{
				throw new NullPointerException("Properties cannot be null!");
			}
			
			this.server = configureServer(properties, logger);
			
			logger.info("Server configured with success!");
			
			server.start();
			
			logger.info("Jetty started with success!");
			
			server.join();
		}
		catch (Exception e) 
		{
			throw new RuntimeException(e); 
		}
	}
	
	/**
	 * Configures the server using the properties file.
	 * 
	 * @return Server
	 * @throws URISyntaxException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws IOException 
	 * @throws UnsupportedTypeException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws ClassNotFoundException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 */
	private Server configureServer(final PropertiesReader properties, final Logger logger) throws InstantiationException, IllegalAccessException, URISyntaxException, IOException, ClassNotFoundException, IllegalArgumentException, InvocationTargetException, UnsupportedTypeException, NoSuchMethodException, SecurityException
	{
		QueuedThreadPool queuedThreadPool = new QueuedThreadPool(Integer.valueOf(properties.getProperty("queued.thread.pool.max.threads")),
																 Integer.valueOf(properties.getProperty("queued.thread.pool.min.threads")),
																 Integer.valueOf(properties.getProperty("queued.thread.pool.idle.timeout")),
								new BlockingArrayQueue<Runnable>(Integer.valueOf(properties.getProperty("queued.thread.pool.queue.capacity")),
																 Integer.valueOf(properties.getProperty("queued.thread.pool.queue.grow.by"))));

		HttpConfiguration httpConfiguration = new HttpConfiguration();
		httpConfiguration.setSecureScheme(properties.getProperty("http.configuration.secure.scheme"));
		httpConfiguration.setSecurePort(Integer.valueOf(properties.getProperty("http.configuration.secure.port")));
		httpConfiguration.setOutputBufferSize(Integer.valueOf(properties.getProperty("http.configuration.output.buffer.size")));
		httpConfiguration.setRequestHeaderSize(Integer.valueOf(properties.getProperty("http.configuration.request.header.size")));
		httpConfiguration.setResponseHeaderSize(Integer.valueOf(properties.getProperty("http.configuration.response.header.size")));
		httpConfiguration.setSendServerVersion(Boolean.valueOf(properties.getProperty("http.configuration.send.server.version")));
		httpConfiguration.setSendDateHeader(Boolean.valueOf(properties.getProperty("http.configuration.send.date.header")));
		
		Server server = new Server(queuedThreadPool);
		server.setDumpBeforeStop(false);
		server.setDumpAfterStart(false);
		server.setStopAtShutdown(true);
		
		String hostName = properties.getProperty("server.connector.host");
		Integer port = Integer.valueOf(properties.getProperty("server.connector.port"));
		
		logger.info("Jetty will listen in: {}:{}", hostName, port);
		
		ServerConnector serverConnector = new ServerConnector(server, new HttpConnectionFactory(httpConfiguration));
		serverConnector.setHost(hostName);
		serverConnector.setPort(port);
		serverConnector.setIdleTimeout(Integer.valueOf(properties.getProperty("server.connector.idle.timeout")));
		serverConnector.setAcceptQueueSize(Integer.valueOf(properties.getProperty("server.connector.accept.queue.size")));
		
		server.addConnector(serverConnector);
		
		NCSARequestLog requestLog = new NCSARequestLog();
        requestLog.setFilename(properties.getProperty("request.log.file"));
        requestLog.setFilenameDateFormat(properties.getProperty("request.log.date.format"));
        requestLog.setRetainDays(Integer.valueOf(properties.getProperty("request.log.retain.days")));
        requestLog.setAppend(Boolean.valueOf(properties.getProperty("request.log.append")));
        requestLog.setExtended(Boolean.valueOf(properties.getProperty("request.log.extended")));
        requestLog.setLogCookies(Boolean.valueOf(properties.getProperty("request.log.log.cookies")));
        requestLog.setLogTimeZone(properties.getProperty("request.log.timezone"));
        
        RequestLogHandler requestLogHandler = new RequestLogHandler();
        requestLogHandler.setRequestLog(requestLog);
        
        SessionHandler sessionHandler = new SessionHandler();
        SessionCache sessionCache = new NullSessionCacheFactory().getSessionCache(sessionHandler);
        sessionCache.setSessionDataStore(getSessionDataStore(properties));
        
        sessionHandler.setSessionCache(sessionCache);
        sessionHandler.setHttpOnly(true);
        sessionHandler.setMaxInactiveInterval(
        		Integer.parseInt(properties.getProperty("session.max.inactive.interval")));
                
        HandlerCollection handlers = new HandlerCollection();
        handlers.addHandler(requestLogHandler);
        handlers.addHandler(sessionHandler);
        handlers.addHandler(getServletContextHandler(properties));
        handlers.setServer(server);
        
        server.setHandler(handlers);
        
        return server;
	}

	/**
	 * Instantiate a SessionDataStore according to properties.
	 * 
	 * @param properties
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws ClassNotFoundException
	 * @throws UnsupportedTypeException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	private SessionDataStore getSessionDataStore(PropertiesReader properties) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException, UnsupportedTypeException, NoSuchMethodException, SecurityException
	{
		this.cacheInstance = createCacheInstance(properties);
		SessionDataStore sessionDataStore = createInstance(properties, "session.manager.parameters", "session.manager.data.store");
		((CustomSessionDataStore)sessionDataStore).setCacheInstance(this.cacheInstance);
		return sessionDataStore ;
	}
	
	
	/**
	 * Create a new cache instance.
	 * 
	 * @param properties
	 * @return CacheInstance
	 * @throws ClassNotFoundException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	private CacheInstance createCacheInstance(PropertiesReader properties) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		Class<?> clazz = Class.forName(properties.getProperty("cache.instance.class"));
		Constructor<?> constructor = clazz.getConstructor(Map.class);
		return (CacheInstance)constructor.newInstance(getCacheInstanceMap(properties));
	}
	
	/**
	 * Returns the parsed instances from the configuration file.
	 * 
	 * @param properties
	 * @return Map<String, Integer>
	 */
	private Map<String, Integer> getCacheInstanceMap(PropertiesReader properties)
	{
		String[] instances = properties.getArray("cache.instance.hosts");
		
		if(instances == null || instances.length == 0)
		{
			throw new RuntimeException("No memcached instances found in property: \"memcached.instances\"");
		}
		
		Map<String, Integer> map = new HashMap<>();
		
		for(String instance : instances)
		{
			String[] values = instance.split("\\:");
			
			if(values.length != 2)
			{
				throw new RuntimeException("Invalid \"hostname:port\" value found: \"" + instance + "\"");
			}
			
			map.put(values[0], Integer.parseInt(values[1]));
		}
		
		return map;
	}
	
	/**
	 * Creates a new instance using the file properties.
	 * 
	 * @param properties
	 * @param parametersKey
	 * @param classKey
	 * @return T
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws UnsupportedTypeException
	 * @throws ClassNotFoundException
	 */
	private <T> T createInstance(PropertiesReader properties, String parametersKey, String classKey) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, UnsupportedTypeException, ClassNotFoundException
	{
		@SuppressWarnings("unchecked")
		Class<T> cacheInstanceClass = (Class<T>)Class.forName(properties.getProperty(classKey));
		
		String[] parameterArray = properties.getArray(parametersKey);
		
		T instance = ReflectionUtil.createInstance(cacheInstanceClass, parameterArray);
		
		return instance;
	}
	
	/**
	 * Creates and configures the slf4j logger.
	 * 
	 * @return Slf4jLog
	 */
	private Slf4jLog createSlf4jLog()
	{
		Slf4jLog slf4jLog = new Slf4jLog("Jetty");
		slf4jLog.setDebugEnabled(false);
		
		return slf4jLog;
	}

	/**
	 * Returns the servlet context handler.
	 * 
	 * @param httpServlet
	 * @param servletContextListener
	 * @param resourceBase
	 * @return ServletContextHandler
	 * @throws URISyntaxException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws UnsupportedTypeException 
	 */
	private ServletContextHandler getServletContextHandler(final PropertiesReader properties) throws URISyntaxException, InstantiationException, IllegalAccessException, UnsupportedTypeException
	{
		ServletHolder servletHolder = new ServletHolder();
		
		Boolean enableMultipart = null;
		
		try
		{
			enableMultipart = properties.getCastedProperty(Boolean.class, "multipart.enabled");
		}
		catch(UnsupportedTypeException e)
		{
			getLogger().warn("Error parsing property \"multipart.enabled\", using null as default.");
		}
		
		if(enableMultipart != null && enableMultipart.booleanValue())
		{
			MultipartConfigElement multipartConfigElement = new MultipartConfigElement(
																	properties.getProperty("multipart.temp.dir"), 
																	properties.getCastedProperty(Long.class, "multipart.max.file.size"), 
																	properties.getCastedProperty(Long.class, "multipart.max.request.size"), 
																	properties.getCastedProperty(Integer.class, "multipart.file.size.threshold"));
			
			servletHolder.getRegistration().setMultipartConfig(multipartConfigElement);
		}
		
		servletHolder.setServlet(getHttpServlet());
		
		ServletContextHandler servletContextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
		servletContextHandler.setErrorHandler(null);
		servletContextHandler.setContextPath(CONTEXT_PATH);
		servletContextHandler.addServlet(servletHolder, MAPPING_URL);
		servletContextHandler.addEventListener(getServletContextListener());
		
		List<FilterItem> filters = getFilters();
		
		if(filters != null && filters.size() > 0)
		{
			Logger logger = getLogger();
			
			for(FilterItem filterItem : filters)
			{
				FilterHolder filterHolder = filterItem.getFilterHolder();
				
				logger.info("Adding filter {} in context {}...", filterHolder.getClass().getName(), filterItem.getContext());
				
				servletContextHandler.addFilter(filterHolder, filterItem.getContext(), filterItem.getDispatcherType());
			}
		}

		return servletContextHandler;
	}
	
	/**
	 * Returns a list of filters to used.
	 * 
	 * @return List<FilterItem>
	 */
	protected abstract List<FilterItem> getFilters(); 
	
	/**
	 * Returns the http servlet implementation.
	 * 
	 * @return HttpServlet
	 */
	protected abstract HttpServlet getHttpServlet();
	
	/**
	 * Returns the servlet context listener implementation.
	 * 
	 * @return HttpServlet
	 */
	protected abstract ServletContextListener getServletContextListener();
	
	/**
	 * Returns the current Logger implementation.
	 * 
	 * @return Logger
	 */
	protected abstract Logger getLogger();
}