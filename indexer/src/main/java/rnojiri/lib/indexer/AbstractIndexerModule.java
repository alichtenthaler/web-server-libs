package rnojiri.lib.indexer;

import java.util.List;

import org.slf4j.Logger;

import rnojiri.lib.interfaces.Loggable;
import rnojiri.lib.search.Document;

/**
 * Abstract class to all indexer modules.
 * 
 * @author rnojiri
 *
 * @param <M - model class>
 * @param <D - document class>
 */
public abstract class AbstractIndexerModule<M, D extends Document> implements Loggable
{
	/**
	 * Starts the module.
	 */
	public abstract void start();
	
	/**
	 * Indexes all documents.
	 */
	public void indexAllDocuments()
	{
		Logger log = getLogger();

		log.info("Executing document indexing.");

		int page = 0;
		long docs = 0;

		while (true)
		{
			log.info("Reading page " + page + " from database.");

			List<M> list = getResults(page);

			if (list == null || list.size() == 0)
			{
				break;
			}

			for (M item : list)
			{
				if (indexDocument(log, item))
				{
					docs++;
				}
			}

			page++;
		}

		log.info("No more results were found in database for document indexing in page: " + page);
		log.info("Total of documents indexed: " + docs);
	}
	
	/**
	 * Index the document given the model id.
	 * 
	 * @param logger
	 * @param id
	 * @return boolean
	 */
	public boolean indexDocument(Logger logger, final String id)
	{
		try
		{
			M model = getModelFromDatabase(id);
			
			return indexDocument(logger, model);
		}
		catch (Exception e)
		{
			logger.error("Error loading model.", e);
		}
		
		return false;
	}

	/**
	 * Index a document.
	 * 
	 * @param logger
	 * @param model
	 * @return boolean
	 */
	private boolean indexDocument(Logger logger, M model)
	{
		try
		{
			D document = toDocument(model);
			
			boolean result = sendDocument(document);

			String documentSignature = document.getClass().getName() + "/" + document.getId();

			if (result)
			{
				logger.info("Document indexed with success: " + documentSignature);

				return true;
			}
			else
			{
				logger.error("Error indexing document: " + documentSignature);
			}
		}
		catch (Exception e)
		{
			logger.error("Error indexing document.", e);
		}

		return false;
	}

	/**
	 * Returns a page from the database.
	 * 
	 * @param page
	 * @return List<M>
	 */
	protected abstract List<M> getResults(final int page);

	/**
	 * Converts the database model to document.
	 * 
	 * @param healthPlan
	 * @return V
	 * @throws Exception
	 */
	protected abstract D toDocument(M model) throws Exception;

	/**
	 * Sends the document to Solr.
	 * 
	 * @param document
	 * @return boolean
	 * @throws Exception
	 */
	protected abstract boolean sendDocument(D document) throws Exception;

	/**
	 * Returns the module name.
	 * 
	 * @return String
	 */
	protected abstract String getModuleName();

	/**
	 * Returns a model from database.
	 * 
	 * @param id
	 * @return M
	 */
	protected abstract M getModelFromDatabase(final String id);
}
