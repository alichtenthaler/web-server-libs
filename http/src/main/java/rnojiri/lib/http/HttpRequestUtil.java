package rnojiri.lib.http;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Consts;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.config.RequestConfig.Builder;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rnojiri.lib.support.Constants;

/**
 * Does http requests.
 * 
 * @author rnojiri
 */
public class HttpRequestUtil
{
	private static final Logger LOGGER = LoggerFactory.getLogger(HttpRequestUtil.class);

	private static final String REQUEST_CONCATENATOR_PARAMETER = "&";

	private static final String EQUALS = "=";

	private static final String REQUEST_WITH_PARAMETER_SYMBOL = "?";

	private static final int DEFAULT_TIMEOUT = 10000;

	private static final RequestConfig requestConfig;
	static
	{
		RequestConfig globalConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.DEFAULT).build();

		Builder builder = RequestConfig.copy(globalConfig);

		builder.setCookieSpec(CookieSpecs.IGNORE_COOKIES);
		builder.setConnectionRequestTimeout(DEFAULT_TIMEOUT);
		builder.setConnectTimeout(DEFAULT_TIMEOUT);

		requestConfig = builder.build();
	}

	/**
	 * Does a http get.
	 * 
	 * @param url
	 * @return HttpRequestUtilResponse
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public static HttpRequestUtilResponse get(final String url) throws ClientProtocolException, IOException
	{
		CloseableHttpClient httpclient = HttpClients.createDefault();

		HttpGet httpGet = new HttpGet(url);
		httpGet.setConfig(requestConfig);

		CloseableHttpResponse closeableHttpResponse = httpclient.execute(httpGet);

		int status = closeableHttpResponse.getStatusLine().getStatusCode();
		
		String responseBody = EntityUtils.toString(closeableHttpResponse.getEntity());

		closeableHttpResponse.close();

		httpclient.close();

		return new HttpRequestUtilResponse(status, responseBody);
	}

	/**
	 * Does a http get with query string.
	 * 
	 * @param url
	 * @param parameters
	 * @return HttpRequestUtilResponse
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public static HttpRequestUtilResponse get(final String url, List<NameValuePair> parameters) throws ClientProtocolException, IOException
	{
		StringBuilder sb = new StringBuilder(Constants.BUFFER_SIZE_128).append(url);

		if (parameters != null && parameters.size() > 0)
		{
			sb.append(REQUEST_WITH_PARAMETER_SYMBOL);

			for (int i = 0; i < parameters.size(); i++)
			{
				NameValuePair nameValuePair = parameters.get(i);
				
				sb.append(nameValuePair.getName()).append(EQUALS).append(URLEncoder.encode(nameValuePair.getValue(), Constants.UTF_8));

				if (i != parameters.size()) sb.append(REQUEST_CONCATENATOR_PARAMETER);
			}
		}

		if (LOGGER.isDebugEnabled())
		{
			LOGGER.debug("Url generated for Request [ url= " + sb.toString() + "]");
		}
		
		return HttpRequestUtil.get(sb.toString());
	}
	
	/**
	 * Does a http post with an abstract entity.
	 * 
	 * @param url
	 * @param postParameters
	 * @return HttpRequestUtilResponse
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	private static HttpRequestUtilResponse post(final String url, AbstractHttpEntity entity) throws ClientProtocolException, IOException
	{
		CloseableHttpClient httpclient = HttpClients.createDefault();

		HttpPost httpPost = new HttpPost(url);
		httpPost.setConfig(requestConfig);

		httpPost.setEntity(entity);

		CloseableHttpResponse closeableHttpResponse = httpclient.execute(httpPost);

		int status = closeableHttpResponse.getStatusLine().getStatusCode();
		String responseBody = EntityUtils.toString(closeableHttpResponse.getEntity());

		closeableHttpResponse.close();

		httpclient.close();

		return new HttpRequestUtilResponse(status, responseBody);
	}
	
	/**
	 * Does a http post.
	 * 
	 * @param url
	 * @param postParameters
	 * @return HttpRequestUtilResponse
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public static HttpRequestUtilResponse post(final String url, String content) throws ClientProtocolException, IOException
	{
		return HttpRequestUtil.post(url, new StringEntity(content, Consts.UTF_8));
	}

	/**
	 * Does a http post.
	 * 
	 * @param url
	 * @param postParameters
	 * @return HttpRequestUtilResponse
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public static HttpRequestUtilResponse post(final String url, ArrayList<NameValuePair> postParameters) throws ClientProtocolException, IOException
	{
		return HttpRequestUtil.post(url, new UrlEncodedFormEntity(postParameters, Consts.UTF_8));
	}
}
