package rnojiri.database.transaction.exception;

/**
 * When an exception is throwed inside an atomic transaction.
 * 
 * @author rnojiri
 */
public class AtomicTransactionException extends Exception
{
	private static final long serialVersionUID = 5883171808186281601L;

	/**
	 * @param cause
	 */
	public AtomicTransactionException(Throwable cause)
	{
		super(cause);
	}
}
