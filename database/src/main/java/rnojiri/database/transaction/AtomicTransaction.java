package rnojiri.database.transaction;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import rnojiri.database.transaction.exception.AtomicTransactionException;
import rnojiri.lib.support.CallbackFunction;

/**
 * Executes an atomic transaction.
 * 
 * @author rnojiri
 *
 * @param <R>
 */
public class AtomicTransaction<R, E extends Exception>
{
	private PlatformTransactionManager transactionManager;

	/**
	 * @param transactionManager
	 */
	public AtomicTransaction(PlatformTransactionManager transactionManager)
	{
		this.transactionManager = transactionManager;
	}

	/**
	 * Executes the atomic operation.
	 * 
	 * @param callbackFunction
	 * @return R
	 * @throws AtomicTransactionException
	 */
	public R execute(CallbackFunction<R, E> callbackFunction) throws AtomicTransactionException
	{
		R returnValue = null;
		
		TransactionStatus transactionStatus = null;
		
		try
		{
			transactionStatus = transactionManager.getTransaction(new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRED));
			
			returnValue = callbackFunction.execute();
			
			transactionManager.commit(transactionStatus);
		}
		catch (Exception e)
		{
			transactionManager.rollback(transactionStatus);
			
			throw new AtomicTransactionException(e);
		}
		
		return returnValue;
	}
}
