package rnojiri.lib.shiro.spring;

import java.security.SecureRandom;

import org.apache.shiro.authc.credential.PasswordService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Implements the password service from Shiro using spring's BCrypt.
 * 
 * @author rnojiri
 */
public class BCryptPasswordService implements PasswordService
{
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	
	public BCryptPasswordService()
	{
		bCryptPasswordEncoder = new BCryptPasswordEncoder();
	}
	
	/**
	 * @param strength
	 * @param seed
	 */
	public BCryptPasswordService(int strength, byte seed[])
	{
		bCryptPasswordEncoder = new BCryptPasswordEncoder(strength, new SecureRandom(seed));
	}
	
	@Override
	public String encryptPassword(Object plainTextPassword) throws IllegalArgumentException
	{
		final String rawPassword;
		
		if (plainTextPassword instanceof String)
		{
			rawPassword = (String) plainTextPassword;
		}
		else if(plainTextPassword instanceof char[])
		{
			rawPassword = new String((char[]) plainTextPassword);
		}
		else if (plainTextPassword instanceof CharSequence)
		{
			rawPassword = ((CharSequence) plainTextPassword).toString();
		}
		else
		{
			throw new SecurityException("Unsupported password type: " + plainTextPassword.getClass().getName());
		}
		
		return bCryptPasswordEncoder.encode(rawPassword);
	}

	@Override
	public boolean passwordsMatch(Object submittedPassword, String storedPassword)
	{
		CharSequence charSequence;
		
		if (submittedPassword instanceof String || submittedPassword instanceof CharSequence)
		{
			charSequence = (String)submittedPassword;
		}
		else if(submittedPassword instanceof char[])
		{
			charSequence = new String((char[])submittedPassword);
		}
		else
		{
			throw new SecurityException("Unsupported password type: " + submittedPassword.getClass().getName());
		}
		
		return bCryptPasswordEncoder.matches(charSequence, storedPassword);
	}
}