package rnojiri.lib.shiro.spring;

import java.util.Collection;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ModelAttribute;

import rnojiri.lib.shiro.cache.SessionAttributes;
import rnojiri.lib.support.CommonCodeResults;

/**
 * Adds more functionalities to the spring abstract controller.
 * 
 * @author rnojiri
 */
@Component
public abstract class AbstractController extends rnojiri.lib.spring.controller.AbstractController
{
	public static final String MATTR_USER = "user";
	
	public static final String MATTR_SUBJECT = "subject";
	
	public static final String MATTR_SESSION = "session";
	
	public static final String MATTR_REDIRECT = "redirect";
	
	/**
	 * Returns the thread subject.
	 * 
	 * @return {@link Subject}
	 */
	@ModelAttribute(MATTR_SUBJECT)
	protected Subject getSubject()
	{
		return SecurityUtils.getSubject();
	}
	
	/**
	 * Returns the thread session.
	 * 
	 * @return {@link Session}
	 */
	@ModelAttribute(MATTR_SESSION)
	protected Session getSession()
	{
		return getSubject().getSession();
	}
	
	/**
	 * Returns the thread user.
	 * 
	 * @return {@link User}
	 */
	@SuppressWarnings("unchecked")
	@ModelAttribute(MATTR_USER)
	protected <U> U getUser()
	{
		return (U)getSession().getAttribute(SessionAttributes.USER);
	}
	
	/**
	 * Returns the result code given the collection content.
	 * 
	 * @param collection
	 * @return String
	 */
	protected String getResultCode(Collection<?> collection)
	{
		return (collection == null || collection.isEmpty()) ? CommonCodeResults.EMPTY : CommonCodeResults.SUCCESS;
	}
}
