package rnojiri.lib.shiro.filter;

import javax.servlet.Filter;

/**
 * Filter configuration.
 * 
 * @author rnojiri
 */
public class FilterConfig
{
	private final String name;
	
	private final String context;
	
	private final Filter filter;

	/**
	 * @param name
	 * @param context
	 * @param filter
	 */
	public FilterConfig(String name, String context, Filter filter)
	{
		super();
		this.name = name;
		this.context = context;
		this.filter = filter;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @return the context
	 */
	public String getContext()
	{
		return context;
	}

	/**
	 * @return the filter
	 */
	public Filter getFilter()
	{
		return filter;
	}
}
