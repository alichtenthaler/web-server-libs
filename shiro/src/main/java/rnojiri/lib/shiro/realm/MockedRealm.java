package rnojiri.lib.shiro.realm;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;

/**
 * Returns a mocked Realm to use as a default Realm.
 * 
 * @author rnojiri
 */
public class MockedRealm implements Realm
{	
	@Override
	public String getName()
	{
		return MockedRealm.class.getName();
	}

	@Override
	public boolean supports(AuthenticationToken token)
	{
		return true;
	}

	@Override
	public AuthenticationInfo getAuthenticationInfo(AuthenticationToken token) throws AuthenticationException
	{
		return new AuthenticationInfo()
		{
			private static final long serialVersionUID = -7844830219702612563L;

			@Override
			public PrincipalCollection getPrincipals()
			{				
				return new SimplePrincipalCollection(token.getPrincipal(), getName());
			}
			
			@Override
			public Object getCredentials()
			{
				return token.getCredentials();
			}
		};
	}

}
