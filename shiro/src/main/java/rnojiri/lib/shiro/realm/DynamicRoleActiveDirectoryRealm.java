package rnojiri.lib.shiro.realm;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;
import javax.sql.DataSource;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.realm.ldap.AbstractLdapRealm;
import org.apache.shiro.realm.ldap.LdapContextFactory;
import org.apache.shiro.realm.ldap.LdapUtils;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extends the default Shiro's AbstractLdapRealm to add dynamic role mapping reloading.
 * 
 * @author rnojiri
 */
public class DynamicRoleActiveDirectoryRealm extends AbstractLdapRealm
{
	private static final Logger LOGGER = LoggerFactory.getLogger(DynamicRoleActiveDirectoryRealm.class);

	private ScheduledExecutorService executorService;

	private DataSource dataSource;

	private final String jdbcRoleQuery;

	private final String ldapFilterQuery;

	private final Pattern roleExtractionRegex;

	private Map<String, Set<String>> groupRolesMap;
	
	private Cache<String, LdapUser> ldapUserCache;
	
	private final static String LDAP_USER_CACHE_PREFIX = "LDAP-"; 

	/**
	 * @param reloadTime
	 * @param jdbcRoleQuery
	 * @param ldapFilterQuery
	 * @param roleExtractionRegex
	 * @param dataSource
	 */
	public DynamicRoleActiveDirectoryRealm(long reloadTime, String jdbcRoleQuery, String ldapFilterQuery, String roleExtractionRegex, CacheManager cacheManager, DataSource dataSource)
	{
		super();

		this.dataSource = dataSource;
		this.jdbcRoleQuery = jdbcRoleQuery;
		this.ldapFilterQuery = ldapFilterQuery;
		this.roleExtractionRegex = Pattern.compile(roleExtractionRegex, Pattern.CASE_INSENSITIVE);
		this.groupRolesMap = Collections.emptyMap();
		
		if(cacheManager != null)
		{
			this.setCacheManager(cacheManager);
			this.ldapUserCache = cacheManager.getCache(null);
		}

		executorService = Executors.newScheduledThreadPool(1);
		executorService.scheduleWithFixedDelay(new ReloadJob(), 0, reloadTime, TimeUnit.MILLISECONDS);
	}

	/**
	 * Reloads all mapped roles from the database.
	 * 
	 * @author rnojiri
	 */
	private class ReloadJob implements Runnable
	{
		private static final int GROUP_INDEX = 1;
		private static final int ROLE_INDEX = 2;

		@Override
		public void run()
		{
			Connection connection = null;

			try
			{
				connection = dataSource.getConnection();

				Statement statement = connection.createStatement();

				if (statement.execute(jdbcRoleQuery))
				{
					Map<String, Set<String>> mapping = new HashMap<>();

					ResultSet resultSet = statement.getResultSet();

					while (resultSet.next())
					{
						final String groupName = resultSet.getString(GROUP_INDEX);

						Set<String> roleSet = mapping.get(groupName);

						if (roleSet == null)
						{
							roleSet = new HashSet<>();
						}

						roleSet.add(resultSet.getString(ROLE_INDEX));

						mapping.put(groupName, roleSet);
					}

					if (!mapping.isEmpty())
					{
						LOGGER.info(mapping.size() + " groups were loaded from the database!");

						synchronized (groupRolesMap)
						{
							groupRolesMap = mapping;
						}
					}

					statement.close();
				}
			}
			catch (Exception e)
			{
				LOGGER.error("Error loading roles from database.", e);
			}
			finally
			{
				try
				{
					if (connection != null && !connection.isClosed())
					{
						connection.close();
					}
				}
				catch (Exception e)
				{
					LOGGER.error("Error closing connection.", e);
				}
			}
		}
	}

	@Override
	protected AuthenticationInfo queryForAuthenticationInfo(AuthenticationToken token, LdapContextFactory ldapContextFactory) throws NamingException
	{
		LdapContext ldapContext = null;
		
		LdapUser ldapUserData;

		try
		{
			ldapContext = ldapContextFactory.getLdapContext(token.getPrincipal(), token.getCredentials());
			
			String username = (String)token.getPrincipal();
			
			ldapUserData = getLdapUserData(username, ldapContext);
			
			this.ldapUserCache.put(LDAP_USER_CACHE_PREFIX + username, ldapUserData);
		}
		finally
		{
			LdapUtils.closeContext(ldapContext);
		}

		UsernamePasswordToken upToken = (UsernamePasswordToken) token;

		return new SimpleAuthenticationInfo(upToken.getUsername(), upToken.getPassword(), getName());
	}

	@Override
	protected AuthorizationInfo queryForAuthorizationInfo(PrincipalCollection principals, LdapContextFactory ldapContextFactory) throws NamingException
	{
		String username = (String) getAvailablePrincipal(principals);

		LdapContext ldapContext = ldapContextFactory.getSystemLdapContext();

		LdapUser ldapUserData;

		try
		{
			ldapUserData = getLdapUserData(username, ldapContext);
			
			this.ldapUserCache.put(LDAP_USER_CACHE_PREFIX + username, ldapUserData);
		}
		finally
		{
			LdapUtils.closeContext(ldapContext);
		}

		return new SimpleAuthorizationInfo(ldapUserData.getShiroRoles());
	}

	/**
	 * Get user roles from LDAP.
	 * 
	 * @param username
	 * @param ldapContext
	 * @return Roles
	 * @throws NamingException
	 */
	private LdapUser getLdapUserData(String username, LdapContext ldapContext) throws NamingException
	{
		LdapUser ldapUserData = new LdapUser();

		SearchControls searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		String userPrincipalName = username;

		if (principalSuffix != null)
		{
			userPrincipalName += principalSuffix;
		}

		Object[] searchArguments = new Object[] { userPrincipalName };

		NamingEnumeration<SearchResult> answer = ldapContext.search(searchBase, ldapFilterQuery, searchArguments, searchCtls);

		while (answer.hasMoreElements())
		{
			SearchResult searchResult = (SearchResult) answer.next();

			if (LOGGER.isDebugEnabled())
			{
				LOGGER.debug("Retrieving group names for user [" + searchResult.getName() + "]");
			}

			Attributes attrs = searchResult.getAttributes();

			if (attrs != null)
			{
				NamingEnumeration<? extends Attribute> en = attrs.getAll();

				while (en.hasMore())
				{
					Attribute attr = (Attribute) en.next();

					if (attr.getID().equals("memberOf"))
					{
						Collection<String> groupNames = LdapUtils.getAllAttributeValues(attr);

						if (LOGGER.isDebugEnabled())
						{
							LOGGER.debug("Groups found for user [" + username + "]: " + groupNames);
						}

						addRoles(groupNames, ldapUserData);
					}
					else if (attr.getID().equals("uid"))
					{
						ldapUserData.setLogin((String)attr.get());
					}
					else if (attr.getID().equals("mail"))
					{
						ldapUserData.setMail((String)attr.get());
					}
					else if (attr.getID().equals("name"))
					{
						ldapUserData.setName((String)attr.get());
					}
				}
			}
		}

		return ldapUserData;
	}

	/**
	 * Extract roles from the "memberOf" property.
	 * 
	 * @param groupNames
	 * @param roles
	 */
	private void addRoles(Collection<String> groupNames, LdapUser roles)
	{
		if (groupRolesMap != null)
		{
			for (String groupName : groupNames)
			{
				Matcher matcher = roleExtractionRegex.matcher(groupName);
				
				if (!matcher.find()) continue;
				
				final String ldapRoleName = matcher.group(1);
				
				Set<String> groupRoles = groupRolesMap.get(ldapRoleName);
				
				if (groupRoles != null && !groupRoles.isEmpty())
				{
					for(String role : groupRoles)
					{
						if (LOGGER.isDebugEnabled())
						{
							LOGGER.debug("User is member of group [" + groupName + "], adding role [" + role + "]");
						}
	
						roles.addShiroRole(role);
						roles.addLdapRole(ldapRoleName);
					}
				}
			}
		}
	}
	
	/**
	 * Returns the cached LDAP user.
	 * 
	 * @param email
	 * @return LdapUserData
	 */
	public LdapUser getLdapUser(String email)
	{
		return this.ldapUserCache.get(LDAP_USER_CACHE_PREFIX + email);
	}
}
