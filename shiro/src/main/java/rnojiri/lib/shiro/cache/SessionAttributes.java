package rnojiri.lib.shiro.cache;

/**
 * Defines commons session attributes.
 * 
 * @author rnojiri
 */
public class SessionAttributes
{
	public static final String USER = "user";
	
	public static final String REFERRER = "referrer";
}
